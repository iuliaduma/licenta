function displayDoctorNameTextbox() {
    $('.form-check div.doctorName').html('<label for="doctorName" class="control-label"> Doctor Name </label>\n' +
        '            <input type="text" name="doctorName" class="form-control" id="doctorName" placeholder="Enter doctor\'s name..."\n>')
}
function displayDoctorTypeTextbox(doctorTypes) {
    $('.form-check div.doctorType').html('<label for="doctorType" class="control-label"> Doctor Type </label>\n' +
        '            <input type="text" name="doctorType" class="form-control" id="doctorType" placeholder="Enter doctor\'s type..."\n>')
}