<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Title</title>
    <spring:url value="/resources/css/bootstrap.min.css" var="mainCss"/>
    <link href="${mainCss}" rel="stylesheet"/>
    <spring:url value="/resources/css/loginsignupbuttons.css" var="buttons"/>
    <link href="${buttons}" rel="stylesheet">
    <spring:url value="/resources/css/menu.css" var="menu"/>
    <link href="${menu}" rel="stylesheet">
    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/></head>
</head>
<body>
<div>
    <spring:url value="/resources/images" var="imagePath"/>
    <jsp:include page="WEB-INF/jsp/menu.jsp" />
    <!--<div class="container" style="text-align: right ; padding: 1%">-->
        <!--<nav class="nav" style="text-align: right ; padding: 1%">
        <a href=<c:url value="/login"/>>
            <button class="btn" name="login">Login</button>
        </a>
        <a href=<c:url value="/patients/signup"/>>
            <button class="btn" name="signup">Sign up</button>
        </a>
        </nav>-->
    <!--</div>-->
</div>
</body>
</html>
