<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page session="false" %>
<html>
<head>
    <title>Home</title>
    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/>
</head>
<body>
<spring:url value="/resources/images/" var="imagePath"/>
<jsp:include page="menu.jsp" />
<div class="menu-container" style="text-align: center; vertical-align: center">${errorMsg}</div>
</body>
</html>