<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="div" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Title</title>
    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/>
</head>
<body>
<div class="container" style="background-color: #f1f2ed;">
    <c:url var="addHospital" value="/hospitals/addHospital"/>
    <form:form action="${addHospital}" method="post" modelAttribute="hospital">

        <div class="form-group required">
            <label for="name" class="control-label"> Name </label>
            <form:input path="name" type="text" name="name" class="form-control" id="name" placeholder="Enter hospital's name..."
                   required="true"/>
        </div>

        <div class="form-group required">
            <label for="code" class="control-label"> Code </label>
            <form:input path="code" type="text" name="code" class="form-control" id="code" placeholder="Enter hospital's name..."
                        required="true"/>
        </div>

        <div>Hospital's Adrress</div>
        <div class="form-group required">
            <label for="country" class="control-label"> Country </label>
            <form:input path="address.country" type="text" name="country" class="form-control" id="country" placeholder="Enter hospital's country..."
                   required="true"/>
        </div>

        <div class="form-group required">
            <label for="county" class="control-label"> County </label>
            <form:input path="address.county" type="text" name="county" class="form-control" id="country" placeholder="Enter hospital's county..."
                        required="true"/>
        </div>

        <div class="form-group required">
            <label for="city" class="control-label"> City </label>
            <form:input  path="address.city" type="text" name="city" class="form-control" id="city" placeholder="Enter hospital's city..."
                         required="true"/>
        </div>

        <div class="form-group required">
            <label for="streetName" class="control-label"> Street Name </label>
            <form:input  path="address.streetName" type="text" name="streetName" class="form-control" id="streetName" placeholder="Enter hospital's street name..."
                         required="true"/>
        </div>

        <div class="form-group required">
            <label for="streetNumber" class="control-label"> Street Number </label>
            <form:input  path="address.streetNumber" type="text" name="streetNumber" class="form-control" id="streetNumber" placeholder="Enter hospital's street number..."
                         required="true"/>
        </div>

        <!-- Button for SUBMIT -->
        <div class="text-center">
            <button type="submit" class="btn btn-primary" style="text-align:center">
                Submit
            </button>
        </div>
    </form:form>
</div>
</body>
</html>
