<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>View recipes</title>
    <spring:url value="/resources/css/bootstrap.min.css" var="mainCss"/>
    <link href="${mainCss}" rel="stylesheet"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="/resources/js/filterby.js"></script>
    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/></head>
<body>
<spring:url value="/resources/images" var="imagePath"/>
<jsp:include page="menu.jsp" />
    <div class="container" style="align-items: center">
        <h1 style="text-align: center">
            View recipes
        </h1>
        <div class="row" style="text-align: right">
            <a href="${pageContext.request.contextPath}/recipes/add">
            <button class="btn btn-success">Add</button>
        </a>
        </div>
        <div >
        <table class="table table-striped table-bordered">
            <tr>
                <th style="text-align: center; vertical-align: middle" >Date</th>
                <th style="text-align: center; vertical-align: middle" colspan="3">Doctor</th>
                <th style="text-align: center; vertical-align: middle" >Recipe Name</th>
                <th style="text-align: center; vertical-align: middle" ></th>
            </tr>
            <c:forEach items="${recipes}" var="recipe">
                <tr>
                    <td style="text-align: center; vertical-align: middle">${recipe.data}</td>
                    <td style="text-align: center; vertical-align: middle">${recipe.doctor.firstname}</td>
                    <td style="text-align: center; vertical-align: middle">${recipe.doctor.surname}</td>
                    <td style="text-align: center; vertical-align: middle">${recipe.doctor.doctorType}</td>
                    <td style="text-align: center; vertical-align: middle">${recipe.name}</td>
                    <td style="text-align: center">
                        <c:url var="recipeUrl" value="/patients/recipes/${recipe.name}"/>
                        <button name="${recipe.name}">
                            <a href="${recipeUrl}">
                                View recipe's details
                            </a>
                        </button>
                    </td>
                </tr>
            </c:forEach>
        </table>
        </div>
    </div>
</body>
</html>
