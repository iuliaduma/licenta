<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link rel="stylesheet" href="/resources/css/jquery.timepicker.min.css">
    <link rel="stylesheet" href="/resources/css/jquery.timepicker.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="/resources/js/jquery.timepicker.min.js"></script>
    <script src="/resources/js/jquery.timepicker.js"></script>
    <script src="/resources/js/timepicker.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
                                                              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <spring:url value="/resources/css/mandatory-sign.css" var="sign"/>
    <link href="${sign}" rel="stylesheet">
    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
    <script src="/resources/js/datepicker.js"></script>
</head>
<body>
<spring:url value="/resources/images" var="imagePath"/>
<jsp:include page="menu.jsp" />
<div class="container">
<c:url var="addUrl" value="/programmings/addProgramming"/>
<form:form id="addForm" action="${addUrl}" method="post" modelAttribute="programming">
    <div style="child-align: middle">
    <div class="row">
    <div class="form-group required">
        <div class="input-group date" data-provide="datepicker">
            <label for="data">Data</label>
            <form:input type="text" path="data" class="form-control" id="data"/>
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
    </div>
    </div>

    <div class="row">
        <div class="form-group required">
            <div class="input-group date" data-provide="timepicker">
                <label for="hour">Hour</label>
                <form:input type="time" path="hour" class="form-control" id="hour" value="09:00"/>
            </div>
        </div>
    </div>

    <div class="text-center">
        <button type="submit" class="btn btn-primary" style="text-align:center">
            Submit
        </button>
    </div>
    </div>
</form:form>
</div>
</body>
</html>
