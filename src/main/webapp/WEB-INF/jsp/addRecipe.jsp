<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="div" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Add Recipe</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
                                   integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <spring:url value="/resources/css/mandatory-sign.css" var="login"/>
    <link href="${login}" rel="stylesheet">
    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
    <script src="/resources/js/datepicker.js"></script>
</head>
<body>
<div class="container" style="background-color: transparent;align-content: center; align-items: center">
    <h1>Add Recipe</h1>
<c:url var="addUrl" value="/recipes/add"/>
<form:form id="addForm" action="${addUrl}" method="post" modelAttribute="recipe">

    <div class="form-group required">
        <div class="input-group date" data-provide="datepicker">
            <label for="data">Data</label>
            <form:input type="text" path="data" class="form-control" id="data"/>
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
    </div>

    <div class="form-group required">
        <label for="doctor" class="control-label">Doctor</label>
        <form:select class="form-control" path="doctor" id="doctor" required="true">
            <form:option value="" label="Please select" disabled="true"/>
            <c:forEach var="doctor" items="${doctors}">
                <form:option value="${doctor.cui}" label="${doctor.firstname} ${doctor.surname}"/>
            </c:forEach>
        </form:select>
    </div>


    <!-- Button for IMAGE -->
    <div class="form-group required">
        <label for="image">Upload recipe</label>
        <form:input type="file" path="name" class="form-control-file" id="image" aria-describedby="fileHelp"
                    accept="image/*"/>
    </div>

    <!-- Small note for user regarding the mandatory fields -->
    <small id="noteToUser" class="form-text text-muted">NOTE: Fields marked with <span style="color:red;"> * </span>
        are mandatory!
    </small>

    <!-- Button for SUBMIT -->
    <div class="text-center">
        <button type="submit" class="btn btn-primary" style="text-align:center">
            Submit
        </button>
    </div>


</form:form>
</div>
</body>
</html>
