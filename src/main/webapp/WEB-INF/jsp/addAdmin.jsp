<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="div" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Title</title>
    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/>
</head>
<body>
<div class="container" style="background-color: #f1f2ed;">
    <c:url var="createUserUrl" value="/user/addAdminUser"/>
    <form action="${createUserUrl}" method="post">

        <div class="form-group required">
            <label for="password" class="control-label"> Password </label>
            <input type="text" name="password" class="form-control" id="password" placeholder="Enter user's password..."
                   required>
        </div>


        <div class="form-group required">
            <label for="email" class="control-label"> Email </label>
            <input type="text" name="email" class="form-control" id="email" placeholder="Enter user's email..."
                   required>
        </div>

        <div class="form-group required">
            <label for="username" class="control-label"> Username </label>
            <input type="text" name="username" class="form-control" id="username" placeholder="Enter user's username..."
                   required>
        </div>

        <!-- Button for SUBMIT -->
        <div class="text-center">
            <button type="submit" class="btn btn-primary" style="text-align:center">
                Submit
            </button>
        </div>
    </form>
</div>
</body>
</html>
