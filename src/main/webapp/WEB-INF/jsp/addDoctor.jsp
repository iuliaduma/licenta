<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="div" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <spring:url value="/resources/css/mandatory-sign.css" var="login"/>
    <link href="${login}" rel="stylesheet">
    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/>
</head>
<body>
<div class="container" style="background-color: transparent;align-content: center; align-items: center">
    <spring:url value="/resources/images" var="imagePath"/>
    <jsp:include page="menu.jsp" />
    <h1>Add Doctor</h1>
    <c:url var="createUserUrl" value="/user/signup"/>
    <form:form action="${createUserUrl}" method="post" modelAttribute="doctor" >
        <div style="child-align: middle">
            <!-- Warning messages from the server (if any) ... -->
            <c:forEach items="${errors}" var="error">
                <div class="alert alert-warning">
                    <strong>Warning! </strong><c:out value="${error.defaultMessage}"/>
                </div>
            </c:forEach>

            <div class="row">
                <!-- Form for NAME -->
                <div class="form-group required col-xs-6">
                    <label for="name" class="control-label"> Name </label>
                    <form:input type="text" path="firstname" class="form-control" id="name" placeholder="Enter your name here..."
                                data-fv-notempty="true"
                                data-fv-notempty-message="A name is required for patient!"
                    />
                </div>
                <div class="form-group required col-xs-6">
                    <label for="surname" class="control-label"> Surname </label>
                    <form:input type="text" path="surname" class="form-control" id="surname" placeholder="Enter your surname here..."
                                data-fv-notempty="true"
                                data-fv-notempty-message="A name is required for patient!"
                    />
                </div>
            </div>

            <div class="row">
                <div class="form-group required col-xs-6">
                    <label for="card-code" class="control-label"> Card Code </label>
                    <form:input type="text" path="cardCode" class="form-control" id="card-code" placeholder="Enter your card code here..."
                                data-fv-notempty="true"
                                data-fv-notempty-message="The card code is required for patient!"
                    />
                </div>
                <div class="form-group required col-xs-6">
                    <label for="cnp" class="control-label"> CNP </label>
                    <form:input type="text" path="CNP" class="form-control" id="cnp" placeholder="Enter your cnp here..."
                                data-fv-notempty="true"
                                data-fv-notempty-message="The cnp is required for patient!"
                    />
                </div>
            </div>

            <div class="row">
                <div class="form-group required col-xs-6">
                    <label for="cui" class="control-label"> CUI </label>
                    <form:input path="cui" type="text" name="cui" class="form-control" id="cui" placeholder="Enter user's CUI..."
                           required="true"/>
                </div>
                <div class="form-group required col-xs-6">
                    <label for="type" class="control-label"> Role </label>
                    <form:input path="doctorType" type="text" name="type" class="form-control" id="type" placeholder="Enter user's role..."
                                required="true"/>
                </div>
            </div>

            <div class="row">
                <!-- Form for EMAIL -->
                <div class="form-group required col-xs-6">
                    <label for="email" class="control-label"> Email </label>
                    <form:input type="email" path="account.email" class="form-control" id="email" placeholder="Enter a valid email address... "
                                data-fv-notempty="true"
                                data-fv-notempty-message="A email is required for patient!"
                                required="true"/>
                </div>
                <!-- Form for USERNAME -->
                <div class="form-group required col-xs-6">
                    <label for="username" class="control-label"> Username </label>
                    <form:input type="username" path="account.username" class="form-control" id="username" placeholder="Enter a valid username... "
                                data-fv-notempty="true"
                                data-fv-notempty-message="A username is required for patient!"
                                required="true"/>
                </div>
            </div>
            <div class="row">
                <!-- Form for PASSWORD -->
                <div class="form-group required col-xs-6">
                    <label for="password" class="control-label"> Password </label>
                    <form:input type="password" path="account.password" class="form-control" id="password" placeholder="Enter your password here..."
                                data-fv-notempty="true"
                                data-fv-notempty-message="A password is required for patient!"
                                required="true" />
                </div>
            </div>
            <!-- Small note for user regarding the mandatory fields -->
            <small id="noteToUser" class="form-text text-muted">NOTE: Fields marked with <span style="color:red;"> * </span>
                are mandatory!
            </small>

            <!-- Button for SUBMIT -->
            <div class="text-center">
                <button type="submit" class="btn btn-primary" style="text-align:center">
                    Submit
                </button>
            </div>
        </div>
    </form:form>
</div>
</body>

</html>
