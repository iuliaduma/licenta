<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>View programmings</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <spring:url value="/resources/css/bootstrap.min.css" var="mainCss"/>
    <link href="${mainCss}" rel="stylesheet"/>
    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/>
</head>

<body>
<spring:url value="/resources/images" var="imagePath"/>
<jsp:include page="menu.jsp" />
    <div class="container" style="align-items: center">
        <h1 style="text-align: center">
            View programmings
        </h1>

        <div style="float: left; width: auto">
        <table class="table table-striped table-bordered">
            <tr>
                <th>Date</th>
                <th>Hour</th>
                <th>Duration</th>
                <th colspan="3">Doctor</th>
                <th>Programming Name</th>
                <th></th>
            </tr>
            <c:forEach items="${programmings}" var="programming">
                <tr>
                    <td>${programming.data}</td>
                    <td>${programming.hour}</td>
                    <td>${programming.duration}</td>
                    <td>${programming.doctor.firstname}</td>
                    <td>${programming.doctor.surname}</td>
                    <td>${programming.doctor.doctorType}</td>
                    <td>${programming.name}</td>
                    <td style="text-align: center">
                        <c:url var="programmingUrl" value="/patients/programmingList/${programming.name}"/>
                        <button name="${programming.name}">
                            <a href="${programmingUrl}">
                                View programmings's details
                            </a>
                        </button>
                    </td>
                </tr>
            </c:forEach>
        </table>
        </div>
    </div>

</body>
</html>
