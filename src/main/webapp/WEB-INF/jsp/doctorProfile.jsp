<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="div" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Doctor Profile</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="/resources/js/starreview.js"></script>
    <spring:url value="/resources/css/menu.css" var="menu"/>
    <link href="${menu}" rel="stylesheet"/>
    <spring:url value="/resources/css/starreview.css" var="starreview"/>
    <link rel="stylesheet" href="${starreview}"/>
    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/>
</head>
<body>
<spring:url value="/resources/images/" var="imagePath"/>
<jsp:include page="menu.jsp" />
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="content-wrapper">
                <img src="/resources/images/default-doctor.jpg" class="img-thumbnail">
            </div>
        </div>
        <div class="col-md-6">
                <h1 style="color:#2c3e50;"> ${doctor.firstname}  ${doctor.surname}</h1>
        </div>
        <div class="row">
            <c:forEach items="${doctor.documents}" var="document">
            <div class="row">
                    <span class="label label-primary" style="color:#bdc3c7;">${document.name}</span>
            </div>
            </c:forEach>
        </div>
        <div class="row">
                <span class="label label-important" style="color:#3a87ad;">Feedbacks</span>
        </div>
        <c:forEach items="${doctor.feedbacks}" var="feedback">
            <div class="row">
                    <span class="label" style="color:#326dcc;">${feedback.patient.firstname} ${feedback.patient.surname}</span>
                    <span class="label" style="color:#326DCC;">${feedback.rating}</span>
                    <div class="label" style="color:#326DCC;">${feedback.text}</div>
            </div>
        </c:forEach>

        <c:url var="leaveAReview" value="/doctors/doctor/${doctor.cui}"/>
        <form:form action="${leaveAReview}" method="post" modelAttribute="feedback" >
         <div class="row">
            <section class='rating-widget'>

                <!-- Rating Stars Box -->
                <div class='rating-stars text-center'>
                    <ul id='stars'>
                        <li class='star' title='Poor' data-value='1'>
                            <input type="radio" name="rating" value="1" class="star">
                            <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='Fair' data-value='2'>
                            <input type="radio" name="rating" value="2" class="star">
                            <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='Good' data-value='3'>
                            <input type="radio" name="rating" value="3" class="star">
                            <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='Excellent' data-value='4'>
                            <input type="radio" name="rating" value="4" class="star">
                            <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='WOW!!!' data-value='5'>
                            <input type="radio" name="rating" value="5" class="star">
                            <i class='fa fa-star fa-fw'></i>
                        </li>
                    </ul>
                </div>
                <div class='success-box' style="background: transparent">
                    <div class='clearfix'></div>
                    <img alt='tick image' width='32' src='https://i.imgur.com/3C3apOp.png'/>
                    <div class='text-message'></div>
                    <div class='clearfix'></div>
                    <div class="rating" ></div>
                </div>
            </section>

             <div class="row">
                 <label for="textF">Leave a comment</label>
                 <form:textarea path="text" id="textF" class="form-control" rows="3"/>
             </div>

         </div>
        </div>


            <div class="text-center">
                <button type="submit" class="btn btn-primary" style="text-align:center">
                    Add Review
                </button>
            </div>
        </form:form>

    </div>
</div>

</body>
</html>
