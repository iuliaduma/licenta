<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Hospital List</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/>
</head>
<body>
<spring:url value="/resources/images" var="imagePath"/>
<jsp:include page="menu.jsp" />
    <div class="container">
        <h1>Hospital List</h1>
        <table class="table table-striped ">
            <tr>
                <th></th>
                <th>Name</th>
                <th colspan="5" style="text-align: center">Address</th>
                <th></th>
            </tr>
            <c:forEach items="${hospitals}" var="hospital">
                <tr id="${hospital.code}">
                    <td></td>
                    <td>${hospital.name}</td>
                    <td>${hospital.address.country}</td>
                    <td>${hospital.address.county}</td>
                    <td>${hospital.address.city}</td>
                    <td>${hospital.address.streetName}</td>
                    <td>${hospital.address.streetNumber}</td>
                    <td style="text-align: center">
                        <c:url var="hospitalUrl" value="/hospitals/hospital/${hospital.code}"/>
                        <button name="${hospital.code}">
                            <a href="${hospitalUrl}">
                                View hospital's doctors
                            </a>
                        </button>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</body>
</html>
