<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Doctor Schedule</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
                                        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/>
</head>
<body>
<spring:url value="/resources/images" var="imagePath"/>
<jsp:include page="menu.jsp" />
    <div class="container" style="width: 100%;">
        <h1 style="text-align: center">
            Doctor Schedule
        </h1>
        <div class="responsive-table center">
            <table class="schedule-table table table-bordered table-striped">
                <tr class="table-header" style="background-color: #dddddd">
                    <th>Hour</th>
                    <th>Monday</th>
                    <th>Tuesday</th>
                    <th>Wednesday</th>
                    <th>Thursday</th>
                    <th>Friday</th>
                    </tr>
                <tr>
                    <td>Start Hour</td>
                    <td>${schedule.startHourMonday}</td>
                    <td>${schedule.startHourTuesday}</td>
                    <td>${schedule.startHourWednesday}</td>
                    <td>${schedule.startHourThursday}</td>
                    <td>${schedule.startHourFriday}</td>
                </tr>
                <tr>
                    <td>Stop Hour</td>
                    <td>${schedule.stopHourMonday}</td>
                    <td>${schedule.stopHourTuesday}</td>
                    <td>${schedule.stopHourWednesday}</td>
                    <td>${schedule.stopHourThursday}</td>
                    <td>${schedule.stopHourFriday}</td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
