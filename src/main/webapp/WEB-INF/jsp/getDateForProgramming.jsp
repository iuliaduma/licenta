<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <div class="container">
        <div class="form-group">
            <div class="input-group date" data-provide="datepicker">
                <label for="data">Data</label>
                <input type="text" class="form-control" id="data" name="data" required/>
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
        </div>

    </div>

</body>
</html>
