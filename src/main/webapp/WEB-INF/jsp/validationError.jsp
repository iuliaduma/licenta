<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Could not validate</title>
    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/>
</head>
<body>

<h1>Your link could not be validated.
Please try again.</h1>

</body>
</html>
