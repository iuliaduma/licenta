<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String name = request.getParameter( "email" );
    session.setAttribute( "email", name );
%>
<html>
<head>
    <title>Login Form</title>
    <link href="<c:url value="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"/>"
          rel="stylesheet">
    <spring:url value="/resources/css/loginPage.css" var="login"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <link href="${login}" rel="stylesheet">
    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/>
    
</head>
<body onload="document.f.j_username.focus();" style="background-color: #2c3e50">
<spring:url value="/resources/images" var="imagePath"/>
<jsp:include page="menu.jsp" />
<div class="container">
    <div class="login-container">
        <div id="output">
            <c:if test="${param.error != null}">
                <p style ="color:#f44268"> Login failed. Check that your username and password are correct</p>
            </c:if>
        </div>
        <div class="avatar">
        </div>
        <div class="form-box">
            <form name = 'f' action='${pageContext.request.contextPath}/j_spring_security_check' method="POST">
                <input name="j_username" type="text" placeholder="username">
                <input name="j_password" type="password" placeholder="password">
                <button class="btn btn-info" type="submit">Login</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>



