<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>


<html>
<head>
    <spring:url value="/resources/css/loginsignupbuttons.css" var="buttons"/>
    <link href="${buttons}" rel="stylesheet">
    <spring:url value="/resources/css/menu.css" var="menu"/>
    <link href="${menu}" rel="stylesheet">

</head>
<body>
<c:url var="logoutUrl" value='/j_spring_security_logout'/>
<c:url var="loginUrl" value='/login'/>
<c:url var="adminAddDoctorUrl" value="/user/signup"/>
<div class="menu-container">
    <nav class="nav" style="text-align: right ; padding: 1%">
        <div class="navbar-header">
            <sec:authentication var="user" property="principal"/>
            <sec:authorize access="(hasRole('PATIENT') or hasRole('DOCTOR') or hasRole('ADMIN')) and isAuthenticated()">
            <h3 style="color:#1f6377">Welcome,${user}<h3>
                </sec:authorize>
        </div>

        <sec:authorize access="(hasRole('PATIENT') or hasRole('ADMIN') or hasRole('DOCTOR')) and isAuthenticated()">
            <button class="btn" name="logout">
                <a href="${logoutUrl}">Log out</a>
            </button>
        </sec:authorize>
        <sec:authorize access="!isAuthenticated()">
            <button class="btn" name="signup">
                <a href="${pageContext.request.contextPath}/patients/signup">Sign up</a>
            </button>
        </sec:authorize>
        <sec:authorize access="!isAuthenticated()">
            <button class="btn" name="login">
                <a href="${loginUrl}">Log in</a>
            </button>
        </sec:authorize>
        <sec:authorize access="hasRole('ADMIN') and isAuthenticated()">
            <button class="btn" name="addnewdoctor">
                <a href="${adminAddDoctorUrl}">Add a new doctor</a>
            </button>
        </sec:authorize>
        <sec:authorize access="hasRole('ADMIN') and isAuthenticated()">
            <button class="btn" name="addnewhospital">
                <a href="${pageContext.request.contextPath}/hospitals/addHospital">Add a new hospital</a>
            </button>
        </sec:authorize>
        <sec:authorize access="hasRole('PATIENT') and isAuthenticated()">
            <button class="btn" name="programmings">
                <a href="${pageContext.request.contextPath}/patients/programmingList">View programmings</a>
            </button>
        </sec:authorize>
        <sec:authorize access="hasRole('PATIENT') and isAuthenticated()">
            <button class="btn" name="program">
                <a href="${pageContext.request.contextPath}/programmings/">Programming</a>
            </button>
        </sec:authorize>
        <sec:authorize access="hasRole('PATIENT') and isAuthenticated()">
            <button class="btn" name="hospitals">
                <a href="${pageContext.request.contextPath}/hospitals/">View hospitals list</a>
            </button>
        </sec:authorize>
        <sec:authorize access="hasRole('PATIENT') and isAuthenticated()">
            <button class="btn" name="familydoctorschedule">
                <a href="${pageContext.request.contextPath}/schedules/familyDoctorSchedule">View family doctor schedule</a>
            </button>
        </sec:authorize>
        <sec:authorize access="hasRole('PATIENT') and isAuthenticated()">
            <button class="btn" name="doctorschedule">
                <a href="${pageContext.request.contextPath}/doctors/viewDoctorList">View doctor profile</a>
            </button>
        </sec:authorize>
        <sec:authorize access="hasRole('PATIENT') and isAuthenticated()">
            <button class="btn" name="recipes">
                <a href="${pageContext.request.contextPath}/patients/recipes">View recipes</a>
            </button>
        </sec:authorize>
    </nav>
</div>

</body>
</html>
