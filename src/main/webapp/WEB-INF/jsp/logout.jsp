<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Logged Out Page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/></head>
<body>
<p> You have logged out </p>
<p style = "color:#0cc4f2"><a href=<c:url value="/login"/>>Back to Login</a></p>
</body>
</html>
