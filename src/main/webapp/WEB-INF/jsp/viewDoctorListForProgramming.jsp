<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Doctor List</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
    <script src="/resources/js/datepicker.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/>
</head>
<body>
<spring:url value="/resources/images" var="imagePath"/>
<jsp:include page="menu.jsp" />
<div class="container">


    <h1>Doctor List</h1>
    <table class="table table-striped ">
        <tr>
            <th></th>
            <th>Name</th>
            <th>Doctor Type</th>
            <th></th>
        </tr>
        <c:forEach items="${doctors}" var="doctor">
            <tr id="${doctor.cui}">
                <td></td>
                <td>${doctor.firstname}  ${doctor.surname}</td>
                <td>${doctor.doctorType}</td>
                <td style="text-align: center">
                    <c:url var="programmingUrl" value="/programmings/doctor/${doctor.cui}"/>
                    <button name="${doctor.cui}">
                        <a href="${programmingUrl}">
                            Programming
                        </a>
                    </button>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
