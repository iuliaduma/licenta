<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Title</title>
    <spring:url value="/resources/css/body.css" var="body"/>
    <link rel="stylesheet" href="${body}"/>
</head>
<body>
<spring:url value="/resources/images" var="imagePath"/>
<jsp:include page="menu.jsp" />
<div class="container" style="align-items: center">
    <h1 style="text-align: center">
        View programmings
    </h1>
    <table class="table table-striped table-bordered">
        <tr>
            <th>Date</th>
            <th>Hour</th>
            <th>Duration</th>
            <th colspan="3">Doctor</th>
            <th>Programming Name</th>
        </tr>
        <c:forEach items="${programmings}" var="programming">
            <tr>
                <td>${programming.data}</td>
                <td>${programming.hour}</td>
                <td>${programming.duration.duration}</td>
                <td>${programming.doctor.firstname}</td>
                <td>${programming.doctor.surname}</td>
                <td>${programming.doctor.doctorType}</td>
                <td>${programming.name}</td>
            </tr>
        </c:forEach>
    </table>
</div>

</body>
</html>
