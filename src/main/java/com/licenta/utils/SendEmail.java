package com.licenta.utils;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class SendEmail {

    private final static String EMAIL_ADDRESS = "licenta.iulia.duma";
    private final static String PASSWORD = "licenta2018";

    private String sendTo;
    private String subject;
    private String text;


    public SendEmail(){}

    public SendEmail(String sendTo, String subject, String text){
        this.sendTo = sendTo;
        this.subject = subject;
        this.text = text;
    }

    public  String sendEmail() {
        String result = "";
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");
        properties.setProperty("mail.smtp.port", "587");

        Session session = Session.getInstance(properties, null);
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(EMAIL_ADDRESS));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(sendTo)); // Change this from personal email
            message.setSubject(this.subject);
            message.setText(text);

            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", "licenta.iulia.duma", PASSWORD);
            transport.sendMessage(message, message.getAllRecipients());

            result = "Email sent with success! Please check your inbox at " + sendTo;
        } catch (Exception e){
            result = "Could not send the email. Please try again.";
        }
        return result;
    }


}
