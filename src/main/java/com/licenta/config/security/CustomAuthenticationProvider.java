package com.licenta.config.security;

import com.licenta.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;

public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private AccountService userService;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();
        String roleToBeFound = userService.getRole(username, password);
        if (roleToBeFound == null) {
            throw new BadCredentialsException("Username not found");
        } else {
            GrantedAuthority role = new SimpleGrantedAuthority(roleToBeFound);
            System.out.println(role.getAuthority());
            grantedAuthorities.add(role);
            return new UsernamePasswordAuthenticationToken(username,password,grantedAuthorities);
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }

    public void setPasswordEncoder(BCryptPasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

}
