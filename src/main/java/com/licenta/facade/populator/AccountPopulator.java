package com.licenta.facade.populator;

import com.licenta.dto.AccountDTO;
import com.licenta.model.classes.Account;
import org.springframework.stereotype.Component;

@Component
public class AccountPopulator {

    public AccountDTO accountModelToDTO(Account account){
        AccountDTO dto = new AccountDTO();
        if(account != null){
            dto.setEmail(account.getEmail());
            dto.setPassword(account.getPassword());
            dto.setRole(account.getRole());
            dto.setUsername(account.getUsername());
        }
        return dto;
    }
}
