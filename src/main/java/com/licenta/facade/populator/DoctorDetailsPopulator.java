package com.licenta.facade.populator;

import com.licenta.dto.DoctorDetailsDTO;
import com.licenta.model.classes.Doctor;
import com.licenta.repository.FeedbackRepository;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class DoctorDetailsPopulator {

    @Autowired
    private DocumentPopulator documentPopulator;
    @Autowired
    private FeedbackRepository repository;


    public DoctorDetailsDTO doctorModelToDTODetails(Doctor doctor) {
        DoctorDetailsDTO model = new DoctorDetailsDTO();
        if ( doctor != null){
            model.setSurname(doctor.getSurname());
            File file;
            try {
                if (doctor.getImage().equals("default.jpg")) {
                    file = new File("/resources/images/default.jpg");
                } else {
                    file = new File("/resources/images/" + doctor.getCUI() + ".jpg");
                }
                DiskFileItem fileItem = new DiskFileItem("file", "image/jpeg", false, file.getName(), (int) file.length(), file.getParentFile());
                fileItem.getOutputStream();
                MultipartFile multipartFile = new CommonsMultipartFile(fileItem);
                model.setImage(multipartFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
            model.setCui(doctor.getCUI());
            model.setFirstname(doctor.getFirstname());
            model.setDoctorType(doctor.getDoctorType());
            model.setDocuments(documentPopulator.documentsModelToDtos(doctor.getDocuments()));
        }
        return model;
    }

    public List<DoctorDetailsDTO> doctorModelsToDTODetails(List<Doctor> doctors){
        List<DoctorDetailsDTO> models = new ArrayList<>();
        if (doctors != null && !doctors.isEmpty())
            for (Doctor doctor : doctors) {
                models.add(doctorModelToDTODetails(doctor));
            }
        return models;
    }
}
