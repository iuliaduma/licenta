package com.licenta.facade.populator;

import com.licenta.dto.DoctorViewProfileDTO;
import com.licenta.model.classes.Doctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DoctorViewProfilePopulator {

    @Autowired
    private AccountPopulator accountPopulator;

    public DoctorViewProfileDTO doctorModelToDTOViewProfile(Doctor doctor) {
        DoctorViewProfileDTO model = new DoctorViewProfileDTO();
        if ( doctor != null){
            model.setSurname(doctor.getSurname());
            model.setImage(doctor.getImage());
            model.setFirstname(doctor.getFirstname());
            model.setDocuments(doctor.getDocuments());
            model.setDoctorType(doctor.getDoctorType());
            model.setAccount(accountPopulator.accountModelToDTO(doctor.getAccount()));
        }
        return model;
    }

    public List<DoctorViewProfileDTO> doctorModelToDTOViewList(List<Doctor> doctors){
        List<DoctorViewProfileDTO> models = new ArrayList<>();
        if (doctors != null && !doctors.isEmpty())
            for (Doctor doctor : doctors) {
                models.add(doctorModelToDTOViewProfile(doctor));
            }
        return models;
    }
}
