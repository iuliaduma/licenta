package com.licenta.facade.populator;

import com.licenta.dto.RecipeDTO;
import com.licenta.model.classes.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RecipePopulator {

    @Autowired
    private DoctorDetailsPopulator doctorDetailsPopulator;
    @Autowired
    private PatientPopulator patientPopulator;

    public RecipeDTO recipeModelToDto(Recipe recipe){
        RecipeDTO dto = new RecipeDTO();
        if( recipe != null){
            dto.setData(recipe.getData());
            dto.setName(recipe.getName());
            dto.setDoctor(doctorDetailsPopulator.doctorModelToDTODetails(recipe.getDoctor()));
            dto.setPatient(patientPopulator.patientModelToDTODetails(recipe.getPatient()));
        }
        return dto;
    }

    public List<RecipeDTO> recipesModelToDtos(List<Recipe> recipes){
        List<RecipeDTO> dtos = new ArrayList<>();
        if( recipes != null && !recipes.isEmpty()){
            for (Recipe recipe : recipes) {
                dtos.add(recipeModelToDto(recipe));
            }
        }
        return dtos;
    }
}
