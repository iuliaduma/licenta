package com.licenta.facade.populator;

import com.licenta.dto.UserLoginDTO;
import com.licenta.model.classes.Doctor;
import com.licenta.model.classes.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserLoginPopulator {

    @Autowired
    private AccountPopulator accountPopulator;

   public UserLoginDTO doctorModelToUserDTOLogin(Doctor doctor){
        UserLoginDTO dto = new UserLoginDTO();
        if(doctor != null){
            dto.setAccount(accountPopulator.accountModelToDTO(doctor.getAccount()));
            dto.setFirstname(doctor.getFirstname());
            dto.setSurname(doctor.getSurname());
        }
        return dto;
    }

    public UserLoginDTO patientModelToUserDTOLogin(Patient patient){
       UserLoginDTO dto = new UserLoginDTO();
       if (patient != null){
           dto.setSurname(patient.getSurname());
           dto.setFirstname(patient.getFirstname());
           dto.setAccount(accountPopulator.accountModelToDTO(patient.getAccount()));
       }
       return dto;
    }
}
