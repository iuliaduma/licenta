package com.licenta.facade.populator;

import com.licenta.dto.PatientAddDTO;
import com.licenta.model.classes.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PatientPopulator {

    @Autowired
    private AccountPopulator accountPopulator;

    public PatientAddDTO patientModelToDTODetails(Patient patient) {
        PatientAddDTO model = new PatientAddDTO();
        if ( patient != null){
            model.setSurname(patient.getSurname());
            model.setAccount(accountPopulator.accountModelToDTO(patient.getAccount()));
            model.setFirstname(patient.getFirstname());
        }
        return model;
    }
}
