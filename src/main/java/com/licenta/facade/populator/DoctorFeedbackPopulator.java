package com.licenta.facade.populator;

import com.licenta.dto.DoctorFeedbackDTO;
import com.licenta.model.classes.Feedback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DoctorFeedbackPopulator {

    @Autowired
    private UserLoginPopulator userLoginPopulator;

    public DoctorFeedbackDTO doctorFeedbackDTOFromFeedback(Feedback feedback){
        DoctorFeedbackDTO dto = new DoctorFeedbackDTO();
        if( feedback != null){
            dto.setRating(feedback.getRating());
            dto.setText(feedback.getText());
            dto.setPatient(userLoginPopulator.patientModelToUserDTOLogin(feedback.getPatient()));
        }
        return dto;
    }

    public List<DoctorFeedbackDTO> doctorFeedbackDTOSFromFeedbacks(List<Feedback> feedbacks){
        List<DoctorFeedbackDTO> dtos = new ArrayList<>();
        if( feedbacks != null && !feedbacks.isEmpty()) {
            for (Feedback feedback : feedbacks) {
                dtos.add(doctorFeedbackDTOFromFeedback(feedback));
            }
        }
        return dtos;
    }
}
