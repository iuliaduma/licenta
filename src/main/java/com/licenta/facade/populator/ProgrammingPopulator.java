package com.licenta.facade.populator;

import com.licenta.dto.ProgrammingDTO;
import com.licenta.model.classes.Programming;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProgrammingPopulator {

    @Autowired
    private DoctorDetailsPopulator doctorDetailsPopulator;

    public ProgrammingDTO programmingModelToDto(Programming programming){
        ProgrammingDTO dto = new ProgrammingDTO();
        if( programming != null){
            dto.setData(programming.getData());
            dto.setName(programming.getName());
            dto.setDoctor(doctorDetailsPopulator.doctorModelToDTODetails(programming.getDoctor()));
            dto.setDuration(30);
            dto.setHour(programming.getHour());
            dto.setPatient(programming.getPatient());
        }
        return dto;
    }

    public List<ProgrammingDTO> programmingsModelToDtos(List<Programming> programmings){
        List<ProgrammingDTO> dtos = new ArrayList<>();
        if(programmings != null && !programmings.isEmpty()){
            for (Programming programming : programmings) {
                dtos.add(programmingModelToDto(programming));
            }
        }
        return dtos;
    }
}
