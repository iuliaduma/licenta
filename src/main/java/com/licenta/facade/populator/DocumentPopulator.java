package com.licenta.facade.populator;

import com.licenta.dto.DocumentDTO;
import com.licenta.model.classes.Document;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DocumentPopulator {

    public DocumentDTO documentModelToDto(Document document){
        DocumentDTO dto = new DocumentDTO();
        if( document != null){
            dto.setDoctor(document.getDoctor());
            dto.setName(document.getName());
        }
        return dto;
    }

    public List<DocumentDTO> documentsModelToDtos(List<Document> documents){
        List<DocumentDTO> dtos = new ArrayList<>();
        if(documents != null && !documents.isEmpty()){
            for (Document document : documents) {
                dtos.add(documentModelToDto(document));
            }
        }
        return dtos;
    }
}
