package com.licenta.facade.populator;

import com.licenta.dto.HospitalDTO;
import com.licenta.model.classes.Hospital;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class HospitalPopulator {

    public HospitalDTO hospitalModelToDTO(Hospital hospital){
        HospitalDTO dto = new HospitalDTO();
        if(hospital != null){
            dto.setAddress(hospital.getAddress());
            dto.setCode(hospital.getCode());
            dto.setName(hospital.getName());
        }
        return dto;
    }

    public List<HospitalDTO> hospitalModelToDTOs(List<Hospital> hospitals){
        List<HospitalDTO> dtos = new ArrayList<>();
        if(hospitals != null && !hospitals.isEmpty()){
            for (Hospital hospital : hospitals) {
                dtos.add(hospitalModelToDTO(hospital));
            }
        }
        return dtos;
    }
}
