package com.licenta.facade;

import com.licenta.dto.DoctorDetailsDTO;
import com.licenta.facade.reversepopulator.DoctorDetailsReversePopulator;
import com.licenta.model.classes.Doctor;
import com.licenta.model.classes.Patient;
import com.licenta.model.classes.Schedule;
import com.licenta.service.AccountService;
import com.licenta.service.DoctorService;
import com.licenta.service.PatientService;
import com.licenta.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ScheduleFacade {

    @Autowired
    private AccountService accountService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private PatientService patientService;
    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private DoctorDetailsReversePopulator doctorDetailsReversePopulator;


    public Schedule getFamilyDoctorSchedule(String user){
        Patient patient = patientService.getPatient(accountService.getAccount(user));
        return scheduleService.findById(patient.getFamilyDoctor().getSchedule().getId());
    }

    public Schedule getDoctorSchedule(DoctorDetailsDTO doctorDTO){
        Doctor doctor =  doctorService.getDoctorByName(doctorDetailsReversePopulator.detailsDtoToModel(doctorDTO).getFirstname() + " " + doctorDetailsReversePopulator.detailsDtoToModel(doctorDTO).getSurname());
        if(doctor != null)
            return doctorService.getDoctorWeeklySchedule(doctor.getId());
        return null;
    }

}
