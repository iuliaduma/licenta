package com.licenta.facade;

import com.licenta.dto.DoctorDetailsDTO;
import com.licenta.dto.RecipeDTO;
import com.licenta.facade.populator.DoctorDetailsPopulator;
import com.licenta.facade.reversepopulator.RecipeReversePopulator;
import com.licenta.model.classes.Doctor;
import com.licenta.model.classes.Patient;
import com.licenta.model.classes.Recipe;
import com.licenta.service.AccountService;
import com.licenta.service.DoctorService;
import com.licenta.service.PatientService;
import com.licenta.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RecipeFacade {

    @Autowired
    private AuthenticationFacade authenticationFacade;
    @Autowired
    private RecipeService recipeService;
    @Autowired
    private PatientService patientService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private RecipeReversePopulator recipeReversePopulator;
    @Autowired
    private DoctorDetailsPopulator doctorDetailsPopulator;

    public Integer add(RecipeDTO recipeDTO){
        Doctor doctor = doctorService.getDoctorByCui(recipeDTO.getDoctor().getCui());
        Patient patient = patientService.getPatient(accountService.getAccountByEmail(authenticationFacade.verifyIfAuthenticatedAndThenRetrieveName()));
        Recipe recipe = recipeReversePopulator.recipeDtoToModelInsertion(recipeDTO);
        recipe.setPatient(patient);
        recipe.setDoctor(doctor);
        return recipeService.create(recipe);
    }

    public List<DoctorDetailsDTO> getDoctors(){
        return doctorDetailsPopulator.doctorModelsToDTODetails(doctorService.getAll());
    }
}
