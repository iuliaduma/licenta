package com.licenta.facade;

public interface InterfaceAuthenticationFacade {
    String verifyIfAuthenticatedAndThenRetrieveName();
}
