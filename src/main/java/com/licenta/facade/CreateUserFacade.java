package com.licenta.facade;

import com.licenta.dto.AccountDTO;
import com.licenta.dto.DoctorAddDTO;
import com.licenta.dto.HospitalDTO;
import com.licenta.dto.PatientAddDTO;
import com.licenta.facade.populator.HospitalPopulator;
import com.licenta.facade.reversepopulator.AccountReversePopulator;
import com.licenta.facade.reversepopulator.DoctorAddReversePopulator;
import com.licenta.facade.reversepopulator.PatientAddReversePopulator;
import com.licenta.model.classes.Account;
import com.licenta.model.classes.Doctor;
import com.licenta.model.classes.Patient;
import com.licenta.model.enums.AccountRole;
import com.licenta.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;


@Component
public class CreateUserFacade {

    @Autowired
    private AccountService accountService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private PatientService patientService;
    @Autowired
    private HospitalService hospitalService;
    @Autowired
    AccountReversePopulator accountReversePopulator;
    @Autowired
    DoctorAddReversePopulator doctorAddReversePopulator;
    @Autowired
    PatientAddReversePopulator patientAddReversePopulator;
    @Autowired
    HospitalPopulator hospitalPopulator;
    @Autowired
    private ScheduleService scheduleService;

    public Integer addAccount(AccountDTO accountDTO) {
        Account account = accountReversePopulator.accountDtoToModelInsertion(accountDTO);
        return accountService.create(account);
    }

    public Integer addDoctor(DoctorAddDTO doctorAddDTO){
        Doctor doctor = doctorAddReversePopulator.doctorDtoToModelInsertion(doctorAddDTO);
        //accountService.create(doctor.getAccount());
        doctor.getAccount().setRole(AccountRole.DOCTOR);
        doctor.setHospital(hospitalService.getHospitalByCode("1"));
        doctor.setSchedule(scheduleService.findById(1));
        //doctor.setHospital(hospitalService.getHospitalByCode(doctor.getHospital().getCode() ));
        return doctorService.create(doctor);
    }

    public  Integer addPatient(PatientAddDTO patientAddDTO, AccountDTO accountDTO){
        Patient patient =  patientAddReversePopulator.patientDtoToModelInsertion(patientAddDTO);
        //List<Account> accounts= new ArrayList<>();
        //addAccount(accountDTO);
        //accounts.add(accountReversePopulator.accountDtoToModelInsertion(accountDTO));
        return patientService.create(patient);
    }

    public Integer getIdForAccountByEmail(String email){
        return accountService.getIdForAccountWithEmail(email);
    }

    public Integer createAdminAccount(AccountDTO accountDTO){
        Account account = accountReversePopulator.accountDtoToModelAdminInsertion(accountDTO);
        String token = UUID.randomUUID().toString();

        account.setToken(token);
        accountService.sendConfirmationEmail(account);
        
        return accountService.create(account);
    }

    public boolean enableAccount(String token){
        return accountService.enableAccount(token);
    }

    public List<Account> getAll(){return  accountService.getAll();}

    public List<HospitalDTO> getAllHospitals(){
        return hospitalPopulator.hospitalModelToDTOs(hospitalService.getAll());
    }
}
