package com.licenta.facade;

import com.licenta.dto.DoctorDetailsDTO;
import com.licenta.dto.DoctorFeedbackDTO;
import com.licenta.facade.populator.DoctorDetailsPopulator;
import com.licenta.facade.populator.DoctorFeedbackPopulator;
import com.licenta.facade.reversepopulator.AccountReversePopulator;
import com.licenta.facade.reversepopulator.DoctorFeedbackReversePopulator;
import com.licenta.model.classes.Doctor;
import com.licenta.model.classes.Feedback;
import com.licenta.service.AccountService;
import com.licenta.service.DoctorService;
import com.licenta.service.FeedbackService;
import com.licenta.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DoctorFacade {

    @Autowired
    private DoctorService doctorService;
    @Autowired
    private DoctorDetailsPopulator doctorDetailsPopulator;
    @Autowired
    private FeedbackService feedbackService;
    @Autowired
    private AccountService accountService;
    @Autowired
    DoctorFeedbackPopulator doctorFeedbackPopulator;
    @Autowired
    DoctorFeedbackReversePopulator doctorFeedbackReversePopulator;
    @Autowired
    private PatientService patientService;
    @Autowired
    private AccountReversePopulator accountReversePopulator;
    @Autowired
    private AuthenticationFacade authenticationFacade;

    public List<DoctorDetailsDTO> getDoctors(){
       return doctorDetailsPopulator.doctorModelsToDTODetails(doctorService.getAll());
    }

    public DoctorDetailsDTO getDoctor(DoctorDetailsDTO dto){
        return doctorDetailsPopulator.doctorModelToDTODetails(doctorService.getDoctorByName(dto.getFirstname() + " " + dto.getSurname()));
    }

    public DoctorDetailsDTO getDoctor(String cui){
        Doctor d = doctorService.getDoctorByCui(cui);
        DoctorDetailsDTO doctor = doctorDetailsPopulator.doctorModelToDTODetails(d);
        doctor.setFeedbacks(doctorFeedbackPopulator.doctorFeedbackDTOSFromFeedbacks(feedbackService.getFeedbacksForDoctor(doctorService.getDoctorByCui(cui).getId())));
        return doctor;
    }

    public void addFeedback(DoctorFeedbackDTO feedbackDTO, String cui){
        Doctor doctor = doctorService.getDoctorByCui(cui);
        Feedback feedback = doctorFeedbackReversePopulator.doctorFeedbackDTOToModel(feedbackDTO);
        feedback.setDoctor(doctor);
        feedback.setPatient(patientService.getPatient(accountService.getAccount(authenticationFacade.verifyIfAuthenticatedAndThenRetrieveName())));
        feedbackService.create(feedback);
    }
}
