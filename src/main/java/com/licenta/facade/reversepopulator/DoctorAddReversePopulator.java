package com.licenta.facade.reversepopulator;

import com.licenta.dto.DoctorAddDTO;
import com.licenta.model.classes.Doctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DoctorAddReversePopulator {

    @Autowired
    private AccountReversePopulator accountReversePopulator;
    @Autowired
    private DocumentReversePopulator documentReversePopulator;

    public Doctor doctorDtoToModelInsertion(DoctorAddDTO model) {
        Doctor doctor = new Doctor();
        if ( model != null){
            doctor.setCUI(model.getCui());
            doctor.setSurname(model.getSurname());
            doctor.setImage(model.getImage());
            doctor.setFirstname(model.getFirstname());
            doctor.setDocuments(documentReversePopulator.documentDtosToModels(model.getDocuments()));
            doctor.setDoctorType(model.getDoctorType());
            doctor.setCNP(model.getCNP());
            doctor.setCardCode(model.getCardCode());
            //List<Account> accounts = new ArrayList<>();
            //accounts.add(accountReversePopulator.accountDtoToModelInsertion(model.getAccount()));
            doctor.setAccount(accountReversePopulator.accountDtoToModelInsertion(model.getAccount()));
        }
        return doctor;
    }
}
