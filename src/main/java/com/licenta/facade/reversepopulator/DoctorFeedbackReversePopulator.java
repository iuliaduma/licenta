package com.licenta.facade.reversepopulator;

import com.licenta.dto.DoctorFeedbackDTO;
import com.licenta.model.classes.Feedback;
import com.licenta.model.enums.FeedbackType;
import org.springframework.stereotype.Component;

@Component
public class DoctorFeedbackReversePopulator {

    public Feedback doctorFeedbackDTOToModel(DoctorFeedbackDTO dto){
        Feedback feedback = new Feedback();
        if( dto != null){
            feedback.setText(dto.getText());
            feedback.setRating(dto.getRating());
            feedback.setFeedbackType(FeedbackType.DOCTOR);
        }
        return feedback;
    }
}
