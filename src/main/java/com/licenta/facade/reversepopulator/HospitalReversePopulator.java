package com.licenta.facade.reversepopulator;

import com.licenta.dto.HospitalDTO;
import com.licenta.model.classes.Hospital;
import org.springframework.stereotype.Component;

@Component
public class HospitalReversePopulator {

    public Hospital hospitalDTOToModel(HospitalDTO dto){
        Hospital hospital = new Hospital();
        if (dto != null){
            hospital.setAddress(dto.getAddress());
            hospital.setName(dto.getName());
            hospital.setCode(dto.getCode());
        }
        return hospital;
    }
}
