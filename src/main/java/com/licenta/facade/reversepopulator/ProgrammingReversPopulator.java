package com.licenta.facade.reversepopulator;

import com.licenta.dto.ProgrammingDTO;
import com.licenta.model.classes.Programming;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProgrammingReversPopulator {

    @Autowired
    private DoctorDetailsReversePopulator doctorDetailsReversePopulator;

    public Programming programmingDtoToModel(ProgrammingDTO dto){
        Programming programming = new Programming();
        if (dto != null){
            programming.setName(dto.getName());
            programming.setData(dto.getData());
            programming.setDoctor(doctorDetailsReversePopulator.detailsDtoToModel(dto.getDoctor()));
            programming.setDuration(dto.getDuration());
            programming.setHour(dto.getHour());
        }
        return programming;
    }
}
