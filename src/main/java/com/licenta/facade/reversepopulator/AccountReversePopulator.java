package com.licenta.facade.reversepopulator;

import com.licenta.dto.AccountDTO;
import com.licenta.model.classes.Account;
import com.licenta.model.enums.AccountRole;
import org.springframework.stereotype.Component;

@Component
public class AccountReversePopulator {

    public Account accountDtoToModelInsertion(AccountDTO model) {
        Account account = new Account();
        if (model != null) {
            account.setUsername(model.getUsername());
            account.setEmail(model.getEmail());
            account.setPassword(model.getPassword());
            account.setEnabled(1);
            account.setRole(model.getRole());
        }
        return account;
    }

    public Account accountDtoToModelAdminInsertion(AccountDTO model){
        Account account = new Account();
        if (model != null) {
            account.setUsername(model.getUsername());
            account.setEmail(model.getEmail());
            account.setPassword(model.getPassword());
            account.setEnabled(1);
            account.setRole(AccountRole.ADMIN);
        }
        return account;
    }
}
