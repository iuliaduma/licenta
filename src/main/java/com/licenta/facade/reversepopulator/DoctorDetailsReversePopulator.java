package com.licenta.facade.reversepopulator;

import com.licenta.dto.DoctorDetailsDTO;
import com.licenta.model.classes.Doctor;
import org.springframework.stereotype.Component;


@Component
public class DoctorDetailsReversePopulator {

    public Doctor detailsDtoToModel(DoctorDetailsDTO dto){
        Doctor doctor = new Doctor();
        if( dto != null){
            doctor.setFirstname(dto.getFirstname());
            doctor.setCUI(dto.getCui());
            doctor.setSurname(dto.getSurname());
            doctor.setDoctorType(dto.getDoctorType());
            doctor.setImage(dto.getImage().getOriginalFilename());
        }
        return doctor;
    }
}
