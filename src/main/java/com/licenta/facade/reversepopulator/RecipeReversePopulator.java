package com.licenta.facade.reversepopulator;

import com.licenta.dto.RecipeDTO;
import com.licenta.model.classes.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RecipeReversePopulator {

    @Autowired
    private PatientAddReversePopulator patientAddReversePopulator;
    @Autowired
    private DoctorDetailsReversePopulator doctorDetailsReversePopulator;

    public Recipe recipeDtoToModelInsertion(RecipeDTO model) {
        Recipe recipe = new Recipe();
        if ( model != null){
            recipe.setData(model.getData());
            recipe.setDoctor(doctorDetailsReversePopulator.detailsDtoToModel(model.getDoctor()));
            recipe.setName(model.getName());
            recipe.setPatient(patientAddReversePopulator.patientDtoToModelInsertion(model.getPatient()));
        }
        return recipe;
    }
}
