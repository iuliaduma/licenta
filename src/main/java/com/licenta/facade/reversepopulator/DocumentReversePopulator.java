package com.licenta.facade.reversepopulator;

import com.licenta.dto.DocumentDTO;
import com.licenta.model.classes.Document;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DocumentReversePopulator {

    public Document documentDtoToModel(DocumentDTO dto){
        Document document = new Document();
        if(dto!= null){
            document.setName(dto.getName());
            document.setDoctor(dto.getDoctor());
        }
        return document;
    }

    public List<Document> documentDtosToModels(List<DocumentDTO> dtos){
        List<Document> documents = new ArrayList<>();
        if(dtos != null && !dtos.isEmpty()){
            for (DocumentDTO dto: dtos) {
                documents.add(documentDtoToModel(dto));
            }
        }
        return documents;
    }
}
