package com.licenta.facade.reversepopulator;

import com.licenta.dto.PatientAddDTO;
import com.licenta.model.classes.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PatientAddReversePopulator {

    @Autowired
    private AccountReversePopulator accountReversePopulator;

    public Patient patientDtoToModelInsertion(PatientAddDTO model) {
        Patient patient = new Patient();
        if ( model != null){
            patient.setSurname(model.getSurname());
            patient.setImage(model.getImage());
            patient.setFirstname(model.getFirstname());
            patient.setCNP(model.getCNP());
            patient.setCardCode(model.getCardCode());
            //List<Account> accounts = new ArrayList<>();
            //accounts.add(accountReversePopulator.accountDtoToModelInsertion(model.getAccount()));
            patient.setAccount(accountReversePopulator.accountDtoToModelInsertion(model.getAccount()));
        }
        return patient;
    }
}
