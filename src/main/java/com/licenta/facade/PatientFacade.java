package com.licenta.facade;

import com.licenta.dto.PatientAddDTO;
import com.licenta.dto.ProgrammingDTO;
import com.licenta.dto.RecipeDTO;
import com.licenta.dto.UserLoginDTO;
import com.licenta.facade.populator.ProgrammingPopulator;
import com.licenta.facade.populator.RecipePopulator;
import com.licenta.facade.populator.UserLoginPopulator;
import com.licenta.facade.reversepopulator.AccountReversePopulator;
import com.licenta.facade.reversepopulator.DoctorDetailsReversePopulator;
import com.licenta.facade.reversepopulator.PatientAddReversePopulator;
import com.licenta.model.classes.*;
import com.licenta.model.enums.AccountRole;
import com.licenta.model.enums.DoctorType;
import com.licenta.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Component
public class PatientFacade {

    @Autowired
    private PatientService patientService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private ProgrammingService programmingService;
    @Autowired
    private RecipeService recipeService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private UserLoginPopulator userLoginPopulator;
    @Autowired
    private ProgrammingPopulator programmingPopulator;
    @Autowired
    private RecipePopulator recipePopulator;
    @Autowired
    private PatientAddReversePopulator patientAddReversePopulator;
    @Autowired
    private AccountReversePopulator accountReversePopulator;
    @Autowired
    private DoctorDetailsReversePopulator doctorDetailsReversePopulator;

    public Integer add(PatientAddDTO patientAddDTO){
        Account account = accountReversePopulator.accountDtoToModelInsertion(patientAddDTO.getAccount());
   //     if(accountService.create(account)>0){
            Patient patient = patientAddReversePopulator.patientDtoToModelInsertion(patientAddDTO);

        patient.getAccount().setRole(AccountRole.PATIENT);
            return patientService.create(patient);
     //   return 0;
    }

    public UserLoginDTO getUserProfile(String user, String password){
        Account account =accountService.getAccount(user, password);
        if(account != null)
            return userLoginPopulator.patientModelToUserDTOLogin(patientService.getPatient(account));
        return null;
    }
    public List<ProgrammingDTO> getProgrammings(String user, String doctor, DoctorType type, Date startDate, Date endDate){
        Account account = accountService.getAccount(user);
        Patient patient = new Patient();
        if(account!=null)
             patient = patientService.getPatient(account);
        List<Programming> programmings;
        if(endDate != null && startDate != null)
            programmings = programmingService.getAllProgrammingsByPatientFilteredByIntervalDate(patient.getId(), startDate, endDate);
        else if(endDate == null && startDate != null)
            programmings = programmingService.getAllProgrammingsByPatientFilteredByDate(patient.getId(), startDate);
        else if(doctor != null)
            programmings = programmingService.getAllProgrammingsByPatientFilteredByDoctorName(patient.getId(), doctorService.getDoctorByName(doctor));
        else if(type!= null)
            programmings = programmingService.getAllProgrammingsByPatientFilteredByDoctorType(patient.getId(), type);
        else
            programmings = programmingService.getAllProgrammingsByPatient(patient.getId());
        return programmingPopulator.programmingsModelToDtos(programmings);
    }

    public void update(Account account){
        accountService.update(account);
    }

    public List<RecipeDTO> getRecipes(String user, String doctor, DoctorType type, Date startDate, Date endDate){
        Patient patient = patientService.getPatient(accountService.getAccount(user));
        List<Recipe> recipes = new ArrayList<>();
        if(endDate != null && startDate != null)
            recipes = recipeService.getAllRecipesByPatientFilteredByIntervalDate(patient.getId(), startDate, endDate);
        else if(endDate == null && startDate != null)
            recipes = recipeService.getAllRecipesByPatientFilteredByDate(patient.getId(), startDate);
        else if(doctor != null) {
            Doctor doctorO = doctorService.getDoctorByName(doctor);
            if(doctorO !=  null)
                recipes = recipeService.getAllRecipesByPatientFilteredByDoctorName(patient.getId(), doctorO);
        }else if(type!= null)
            recipes = recipeService.getAllRecipesByPatientFilteredByDoctorType(patient.getId(), type);
        else recipes = recipeService.getAllRecipesByPatient(patient.getId());
        return recipePopulator.recipesModelToDtos(recipes);
    }

    public List<DoctorType> getAllDoctorsType(){
        return doctorService.getAllDoctorsType();
    }
}
