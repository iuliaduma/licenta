package com.licenta.facade;

import com.licenta.dto.DoctorDetailsDTO;
import com.licenta.dto.HospitalDTO;
import com.licenta.facade.populator.DoctorDetailsPopulator;
import com.licenta.facade.populator.HospitalPopulator;
import com.licenta.facade.reversepopulator.HospitalReversePopulator;
import com.licenta.service.AddressService;
import com.licenta.service.DoctorService;
import com.licenta.service.HospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class HospitalFacade {

    @Autowired
    private HospitalService hospitalService;
    @Autowired
    private DoctorDetailsPopulator doctorDetailsPopulator;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private HospitalPopulator hospitalPopulator;
    @Autowired
    private HospitalReversePopulator hospitalReversePopulator;
    @Autowired
    private AddressService addressService;

    public List<HospitalDTO> getHospitals(){
        return hospitalPopulator.hospitalModelToDTOs(hospitalService.getAll());
    }

    public List<DoctorDetailsDTO> getDoctorsFilterByHospital(int id){
        return doctorDetailsPopulator.doctorModelsToDTODetails(doctorService.getDoctorListFilterByHospital(id));
    }

    public void create(HospitalDTO dto){
        //addressService.create(dto.getAddress());
        hospitalService.create(hospitalReversePopulator.hospitalDTOToModel(dto));
    }
}
