package com.licenta.facade;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationFacade{
    public String verifyIfAuthenticatedAndThenRetrieveName() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getName() == null) {
            return null;
        }
        return authentication.getName();
    }
}
