package com.licenta.facade;

import com.licenta.dto.DoctorDetailsDTO;
import com.licenta.dto.ProgrammingDTO;
import com.licenta.facade.populator.DoctorDetailsPopulator;
import com.licenta.facade.reversepopulator.ProgrammingReversPopulator;
import com.licenta.model.classes.Doctor;
import com.licenta.model.classes.Patient;
import com.licenta.model.classes.Programming;
import com.licenta.model.enums.DoctorType;
import com.licenta.service.AccountService;
import com.licenta.service.DoctorService;
import com.licenta.service.PatientService;
import com.licenta.service.ProgrammingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.List;

@Component
public class ProgrammingFacade {

    @Autowired
    private ProgrammingService programmingService;
    @Autowired
    DoctorDetailsPopulator doctorDetailsPopulator;
    @Autowired
    private ProgrammingReversPopulator programmingReversPopulator;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private PatientService patientService;
    @Autowired
    private AuthenticationFacade authenticationFacade;

    public List<Programming> getAllProgrammingsByPatient(int patientId){
       return programmingService.getAllProgrammingsByPatient(patientId);
    }

    public List<Programming> getAllProgrammingsByPatientFilteredByDate(int patientId, Date date){
        return programmingService.getAllProgrammingsByPatientFilteredByDate(patientId, date);
    }

    public List<Programming> getAllProgrammingsByPatientFilteredByDoctorName(int patientId, Doctor doctor){
        return programmingService.getAllProgrammingsByPatientFilteredByDoctorName(patientId, doctor);
    }

    public DoctorDetailsDTO getDoctor(String cui){
        Doctor d = doctorService.getDoctorByCui(cui);
        DoctorDetailsDTO doctor = doctorDetailsPopulator.doctorModelToDTODetails(d);
        return doctor;
    }

    public List<Programming> getAllProgrammingsByPatientFilteredByDoctorType(int patientId, DoctorType doctor){
        return programmingService.getAllProgrammingsByPatientFilteredByDoctorType(patientId, doctor);
    }

    public List<DoctorDetailsDTO> getDoctors(){
        return doctorDetailsPopulator.doctorModelsToDTODetails(doctorService.getAll());
    }

    public List<Programming> getAllProgrammingsByPatientFilteredByIntervalDate(int patientId, Date startDate, Date endDate){
        return programmingService.getAllProgrammingsByPatientFilteredByIntervalDate(patientId, startDate, endDate);
    }

    public void create(ProgrammingDTO programmingDTO){
        Programming programming = programmingReversPopulator.programmingDtoToModel(programmingDTO);
        Doctor doctor = doctorService.getDoctorByCui(programmingDTO.getDoctor().getCui());
        Patient patient = patientService.getPatient(accountService.getAccountByEmail(authenticationFacade.verifyIfAuthenticatedAndThenRetrieveName()));
        programming.setPatient(patient);
        programmingService.create(programming);
    }

    /*public List<> getDoctorScheduleByData(String cui, Date data){

    }*/
}
