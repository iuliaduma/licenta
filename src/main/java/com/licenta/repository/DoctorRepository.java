package com.licenta.repository;

import com.licenta.model.classes.Account;
import com.licenta.model.classes.Doctor;
import com.licenta.model.classes.Schedule;
import com.licenta.model.enums.DoctorType;
import com.licenta.repository.interfaces.InterfaceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class DoctorRepository  implements InterfaceRepository<Doctor, Integer> {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Doctor findById(Integer entityId) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Doctor.class, entityId);
    }

    @Override
    public List getAll() {
        Session session=sessionFactory.getCurrentSession();
        return session.createQuery("from Doctor", Doctor.class).list();
    }

    @Override
    public Integer create(Doctor entity) {
        Session session=sessionFactory.getCurrentSession();
        Doctor doctor=new Doctor();
        doctor.setAccount(entity.getAccount());
        doctor.getAccount().setPassword(bCryptPasswordEncoder.encode(doctor.getAccount().getPassword()));
        doctor.setCardCode(entity.getCardCode());
        doctor.setCNP(entity.getCNP());
        doctor.setCUI(entity.getCUI());
        doctor.setDoctorType(entity.getDoctorType());
        doctor.setDocuments(entity.getDocuments());
        doctor.setFirstname(entity.getFirstname());
        doctor.setImage(entity.getImage());
        doctor.setSurname(entity.getSurname());
        return (Integer) session.save(doctor);

    }

    @Override
    public void update(Doctor entity) {
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(entity);
    }

    @Override
    public boolean delete(Integer entityId){
        Session session= sessionFactory.getCurrentSession();
        Doctor doctor = session.get(Doctor.class, entityId);
        session.delete(doctor);
        return !session.contains(doctor);
    }


    public Schedule getDoctorWeeklySchedule(int scheduleId){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Schedule where scheduleId =:scheduleId";
        Query query = session.createQuery(hql);
        query.setParameter("scheduleId", scheduleId);
        return (Schedule) query.getSingleResult();
    }

/*
    public List<Interval> getIntervalsByDate(Date data, Doctor doctor){
        Schedule schedule = getDoctorWeeklySchedule(doctor.getSchedule().getId());
        Time startTime;
        Time stopTime;
        Calendar c = Calendar.getInstance();
        c.setTime(data);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        if(dayOfWeek == 1) {
            startTime = schedule.getStartHourMonday();
            stopTime = schedule.getStopHourMonday();
        }
        if(dayOfWeek == 2) {
            startTime = schedule.getStartHourTuesday();
            stopTime = schedule.getStopHourTuesday();
        }
        if(dayOfWeek == 3) {
            startTime = schedule.getStartHourWednesday();
            stopTime = schedule.getStopHourWednesday();
        }
        if(dayOfWeek == 4) {
            startTime = schedule.getStartHourThursday();
            stopTime = schedule.getStopHourThursday();
        }
        if(dayOfWeek == 5){
            startTime =schedule.getStartHourFriday();
            stopTime = schedule.getStopHourFriday();
        }
        if (dayOfWeek > 5)
            return new ArrayList<>();
        List<Interval> intervals = new ArrayList<>();
        while (startTime.getHours() < stopTime.getHours()){
            if(startTime.)
        }
    }*/

    public Doctor getDoctor(Account account){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Doctor where accountId =:accountId";
        Query query =session.createQuery(hql);
        query.setParameter("accountId", account.getId());
        return (Doctor) query.getSingleResult();
    }

    public Doctor getDoctorByName(String name, String surname){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Doctor";
        Query query = session.createQuery(hql, Doctor.class);
        List<Doctor> doctors= query.getResultList();
        for (Doctor doctor : doctors) {
            if (doctor.getFirstname().equals(name) && doctor.getSurname().equals(surname) || doctor.getSurname().equals(name) && doctor.getFirstname().equals(surname))
                return doctor;
        }
        return null;
    }

    public Doctor getDoctorByCui(String cui){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Doctor where CUI =:cui";
        Query query = session.createQuery(hql);
        query.setParameter("cui", cui);
        return (Doctor) query.getSingleResult();
    }

    public List<Doctor> getDoctorsFilterByHospital(int  hospitalId){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Doctor where hospitalId =:hospitalId";
        Query query = session.createQuery(hql);
        query.setParameter("hospitalId", hospitalId);
        return  query.getResultList();
    }

    public List<DoctorType> getAllDoctorsType(){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Doctor",DoctorType.class);
        List<Doctor> doctors = query.getResultList();
        List<DoctorType> doctorTypes = new ArrayList<>();
        for (Doctor doctor : doctors) {
            if(!doctorTypes.contains(doctor.getDoctorType())){
                doctorTypes.add(doctor.getDoctorType());
            }
        }
        return doctorTypes;
    }
}
