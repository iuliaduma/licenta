package com.licenta.repository;

import com.licenta.model.classes.Account;
import com.licenta.model.classes.Feedback;
import com.licenta.repository.interfaces.InterfaceRepository;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FeedbackRepository implements InterfaceRepository<Feedback, Integer> {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Feedback findById(Integer entityId) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Feedback.class, entityId);
    }

    @Override
    public List<Feedback> getAll() {
        Session session = sessionFactory.getCurrentSession();
        List<Feedback> feedbacks = session.createQuery("from Feedback", Feedback.class).list();
        return feedbacks;
    }

    @Override
    public Integer create(Feedback entity) {
        Session session=sessionFactory.getCurrentSession();
        Feedback feedback=new Feedback();
        feedback.setDoctor(entity.getDoctor());
        feedback.setFeedbackType(entity.getFeedbackType());
        feedback.setHospital(entity.getHospital());
        feedback.setPatient(entity.getPatient());
        feedback.setRating(entity.getRating());
        feedback.setText(entity.getText());
        return (Integer) session.save(feedback);
    }

    @Override
    public void update(Feedback entity) {
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(entity);
    }

    @Override
    public boolean delete(Integer entityId) {
        Session session= sessionFactory.getCurrentSession();
        Feedback feedback = session.get(Feedback.class, entityId);
        session.delete(feedback);
        return !session.contains(feedback);
    }

    public List<Feedback> getFeedbacksForDoctor(int doctorId){
        Session session =sessionFactory.getCurrentSession();
        String hql = "from Feedback where doctorId =:doctorId";
        Query query = session.createQuery(hql);
        query.setParameter("doctorId", doctorId);
        return query.getResultList();
    }

    public List<Feedback> getFeedbacksForHospital(int hospitalId){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Feedback where hospitalId =:hospitalId";
        Query query = session.createQuery(hql);
        query.setParameter("hospitalId", hospitalId);
        return query.getResultList();
    }
}
