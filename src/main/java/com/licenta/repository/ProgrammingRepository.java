package com.licenta.repository;

import com.licenta.model.classes.Doctor;
import com.licenta.model.classes.Programming;
import com.licenta.model.enums.DoctorType;
import com.licenta.repository.interfaces.InterfaceRepository;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ProgrammingRepository implements InterfaceRepository<Programming, Integer> {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Programming findById(Integer entityId) {
        Session session=sessionFactory.getCurrentSession();
        return session.get(Programming.class, entityId);
    }

    @Override
    public List<Programming> getAll() {
        List<Programming> programmings;
        Session session=sessionFactory.getCurrentSession();
        programmings = session.createQuery("from Programming",Programming.class).list();
        return  programmings;
}

    @Override
    public Integer create(Programming entity) {
        Session session=sessionFactory.getCurrentSession();
        Programming programming=new Programming();
        programming.setData(entity.getData());
        programming.setDoctor(entity.getDoctor());
        programming.setDuration(entity.getDuration());
        programming.setHour(entity.getHour());
        programming.setPatient(entity.getPatient());
        programming.setName(entity.getName());
        return (Integer) session.save(programming);
    }

    @Override
    public void update(Programming entity) {
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(entity);
    }

    @Override
    public boolean delete(Integer entityId) {
        Session session=sessionFactory.getCurrentSession();
        Programming programming=session.get(Programming.class, entityId);
        session.delete(programming);
        return !session.contains(programming);
    }

    public List<Programming> getAllProgrammingsByPatient(int patientId){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Programming where patientId =:patientId";
        Query query = session.createQuery(hql);
        query.setParameter("patientId", patientId);
        List<Programming> programmingList= query.getResultList();
        return programmingList;
    }

    public List<Programming> getAllProgrammingsByPatientFilteredByDoctorName(int patientId, Doctor doctor){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Programming where patientId =:patientId and doctorId =:doctorId";
        Query query = session.createQuery(hql);
        query.setParameter("patientId", patientId);
        query.setParameter("doctorId", doctor.getId());
        List<Programming> programmingList= query.getResultList();
        return programmingList;
    }

    public List<Programming> getAllProgrammingsByPatientFilteredByDoctorType(int patientId, DoctorType doctorType){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Programming where patientId =:patientId";
        Query query = session.createQuery(hql);
        query.setParameter("patientId", patientId);
        List<Programming> programmings= query.getResultList();
        List<Programming> programmingList = new ArrayList<>();
        for (Programming programming: programmings) {
              if (programming.getDoctor().getDoctorType().equals(doctorType))
            programmingList.add(programming);
        }
        return programmingList;
    }

    public List<Programming> getAllProgrammingsByPatientFilteredByDate(int patientId, Date date){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Programming where patientId =:patientId and date =:date";
        Query query = session.createQuery(hql);
        query.setParameter("patientId", patientId);
        query.setParameter("date", date);
        List<Programming> programmingList= query.getResultList();
        return programmingList;
    }

    public List<Programming> getAllProgrammingsByPatientFilteredByIntervalDate(int patientId, Date startingDate, Date endDate){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Programming where patientId =:patientId and between :startingDate and :endDate";
        Query query = session.createQuery(hql);
        query.setParameter("patientId", patientId);
        query.setParameter("startingDate", startingDate);
        query.setParameter("endDate", endDate);
        List<Programming> programmingList= query.getResultList();
        return programmingList;
    }

    public List<Programming> getAllProgrammingsByDoctorFilteredByDate(int doctorId, Date date){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Programming where doctorId =:doctorId and date =:date";
        Query query = session.createQuery(hql);
        query.setParameter("doctorId", doctorId);
        query.setParameter("date", date);
        List<Programming> programmingList= query.getResultList();
        return programmingList;
    }

    public List<Programming> getAllProgrammingsByDoctorFilteredByIntervalDate(int doctorId, Date startingDate, Date endDate){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Programming where doctorId =:doctorId and between :startingDate and :endDate";
        Query query = session.createQuery(hql);
        query.setParameter("doctorId", doctorId);
        query.setParameter("startingDate", startingDate);
        query.setParameter("endDate", endDate);
        List<Programming> programmingList= query.getResultList();
        return programmingList;
    }


}
