package com.licenta.repository;

import com.licenta.model.classes.Document;
import com.licenta.repository.interfaces.InterfaceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DocumentRepository  implements InterfaceRepository<Document, Integer> {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public Document findById(Integer entityId) {
        Session session =sessionFactory.getCurrentSession();
        return session.get(Document.class, entityId);
    }

    @Override
    public List<Document> getAll() {
        Session session=sessionFactory.getCurrentSession();
        return session.createQuery("from Document", Document.class).list();
    }

    @Override
    public Integer create(Document entity) {
        Session session=sessionFactory.getCurrentSession();
        Document document=new Document();
        document.setDoctor(entity.getDoctor());
        document.setName(entity.getName());
        return (Integer) session.save(document);
    }

    @Override
    public void update(Document entity) {
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(entity);
    }

    @Override
    public boolean delete(Integer entityId) {
        Session session=sessionFactory.getCurrentSession();
        Document document=session.get(Document.class, entityId);
        session.saveOrUpdate(document);
        return !session.contains(document);
    }
}
