package com.licenta.repository;

import com.licenta.model.classes.Doctor;
import com.licenta.model.classes.Recipe;
import com.licenta.model.enums.DoctorType;
import com.licenta.repository.interfaces.InterfaceRepository;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RecipeRepository  implements InterfaceRepository<Recipe, Integer> {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public Recipe findById(Integer entityId) {
        Session session=sessionFactory.getCurrentSession();
        return session.get(Recipe.class, entityId);
    }

    @Override
    public List<Recipe> getAll() {
        Session session=sessionFactory.getCurrentSession();
        return session.createQuery("from Recipe", Recipe.class).list();
    }

    @Override
    public Integer create(Recipe entity) {
        Session session=sessionFactory.getCurrentSession();
        Recipe recipe=new Recipe();
        recipe.setData(entity.getData());
        recipe.setDoctor(entity.getDoctor());
        recipe.setPatient(entity.getPatient());
        recipe.setName(entity.getName());
        return (Integer) session.save(recipe);
    }

    @Override
    public void update(Recipe entity) {
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(entity);
    }

    @Override
    public boolean delete(Integer entityId) {
        Session session=sessionFactory.getCurrentSession();
        Recipe recipe=session.get(Recipe.class,entityId);
        session.delete(recipe);
        return !session.contains(recipe);
    }

    public List<Recipe> getAllRecipesByPatient(int patientId){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Recipe where patientId =:patientId";
        Query query = session.createQuery(hql);
        query.setParameter("patientId", patientId);
        return query.getResultList();
    }

    public List<Recipe> getAllRecipesByPatientFilteredByDoctorName(int patientId, Doctor doctor){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Recipe where patientId =:patientId and doctorId =:doctorId";
        Query query = session.createQuery(hql);
        query.setParameter("patientId", patientId);
        query.setParameter("doctorId", doctor.getId());
        List<Recipe> recipes = query.getResultList();
        return query.getResultList();
    }

    public List<Recipe> getAllRecipesByPatientFilteredByDoctorType(int patientId, DoctorType doctorType){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Recipe where patientId =:patientId";
        Query query = session.createQuery(hql);
        query.setParameter("patientId", patientId);
        List<Recipe> recipes= query.getResultList();
        List<Recipe> recipeList = new ArrayList<>();
        for (Recipe recipe: recipes) {
             if (recipe.getDoctor().getDoctorType().equals(doctorType))
            recipeList.add(recipe);
        }
        return recipeList;
    }

    public List<Recipe> getAllRecipesByPatientFilteredByDate(int patientId, Date date){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Recipe where patientId =:patientId and date =:date";
        Query query = session.createQuery(hql);
        query.setParameter("patientId", patientId);
        query.setParameter("date", date);
        return query.getResultList();
    }

    public List<Recipe> getAllRecipesByPatientFilteredByIntervalDate(int patientId, Date startingDate, Date endDate){
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Recipe where patientId =:patientId and date between :startingDate and :endDate";
        Query query = session.createQuery(hql);
        query.setParameter("patientId", patientId);
        query.setParameter("startingDate", startingDate);
        query.setParameter("endDate", endDate);
        return query.getResultList();
    }
}
