package com.licenta.repository;

import com.licenta.model.classes.Drug;
import com.licenta.repository.interfaces.InterfaceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DrugRepository implements InterfaceRepository<Drug, Integer> {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Drug findById(Integer entityId) {
        Session session=sessionFactory.getCurrentSession();
        return session.get(Drug.class, entityId);
    }

    @Override
    public List<Drug> getAll() {
        Session session=sessionFactory.getCurrentSession();
        return session.createQuery("from Drug", Drug.class).list();
    }

    @Override
    public Integer create(Drug entity) {
        Session session=sessionFactory.getCurrentSession();
        Drug drug= new Drug();
        drug.setCode(entity.getCode());
        drug.setConcentration(entity.getConcentration());
        drug.setDescription(entity.getDescription());
        drug.setName(entity.getName());
        return (Integer) session.save(drug);
    }

    @Override
    public void update(Drug entity) {
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(entity);
    }

    @Override
    public boolean delete(Integer entityId) {
        Session session=sessionFactory.getCurrentSession();
        Drug drug=session.get(Drug.class, entityId);
        session.delete(drug);
        return !session.contains(drug);
    }
}
