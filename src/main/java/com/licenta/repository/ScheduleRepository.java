package com.licenta.repository;

import com.licenta.model.classes.Doctor;
import com.licenta.model.classes.Interval;
import com.licenta.model.classes.Schedule;
import com.licenta.repository.interfaces.InterfaceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.List;

@Component
public class ScheduleRepository implements InterfaceRepository<Schedule, Integer> {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Schedule findById(Integer entityId) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Schedule.class, entityId);
    }

    @Override
    public List<Schedule> getAll() {
        Session session=sessionFactory.getCurrentSession();
        return session.createQuery("from Schedule", Schedule.class).list();
    }

    @Override
    public Integer create(Schedule entity) {
        Session session=sessionFactory.getCurrentSession();
        Schedule schedule=new Schedule();
        schedule.setDoctor(entity.getDoctor());
        schedule.setStartHourFriday(entity.getStartHourFriday());
        schedule.setStartHourMonday(entity.getStartHourMonday());
        schedule.setStartHourTuesday(entity.getStartHourTuesday());
        schedule.setStartHourWednesday(entity.getStartHourWednesday());
        schedule.setStartHourThursday(entity.getStartHourThursday());
        schedule.setStopHourFriday(entity.getStopHourFriday());
        schedule.setStopHourMonday(entity.getStopHourMonday());
        schedule.setStopHourTuesday(entity.getStopHourTuesday());
        schedule.setStopHourWednesday(entity.getStopHourWednesday());
        schedule.setStopHourThursday(entity.getStopHourThursday());
        return (Integer) session.save(schedule);
    }

    @Override
    public void update(Schedule entity) {
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(entity);
    }

    @Override
    public boolean delete(Integer entityId) {
        Session session= sessionFactory.getCurrentSession();
        Schedule schedule = session.get(Schedule.class, entityId);
        session.delete(schedule);
        return !session.contains(schedule);
    }

}
