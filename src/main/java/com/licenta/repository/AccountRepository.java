package com.licenta.repository;

import com.licenta.model.classes.Account;
import com.licenta.model.enums.AccountRole;
import com.licenta.repository.interfaces.InterfaceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class AccountRepository implements InterfaceRepository<Account, Integer> {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Account findById(Integer entityId) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Account.class, entityId);
    }

    @Override
    public List<Account> getAll() {
        Session session = sessionFactory.getCurrentSession();
        List<Account> accounts = session.createQuery("from Account", Account.class).list();
        return accounts;
    }

    @Override
    public Integer create(Account account) {
        Session session= sessionFactory.getCurrentSession();
        Account accountToInsert = new Account();
        accountToInsert.setEmail(account.getEmail());
        accountToInsert.setPassword(bCryptPasswordEncoder.encode(account.getPassword()));
        accountToInsert.setUsername(account.getUsername());
        accountToInsert.setRole(account.getRole());
        return (Integer) session.save(accountToInsert);
    }

    @Override
    public void update(Account account) {
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(account);
    }

    @Override
    public boolean delete(Integer entityId) {
        Session session= sessionFactory.getCurrentSession();
        Account account = session.get(Account.class, entityId);
        session.delete(account);
        return !session.contains(account);
    }

    public Account getAccountByUsername(String username) {
        Account soughtAccount;
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Account where username =:username";
        Query query = session.createQuery(hql);
        query.setParameter("username", username);
        try {
            soughtAccount = (Account) query.getSingleResult();
        } catch (NoResultException nre) {
            soughtAccount = null;
        }
        return soughtAccount;
    }

    public Integer getIdForAccountWithUsername(String username) {
        return getAccountByUsername(username).getId();
    }

    public Account getAccountByEmail(String email){
        Account soughtAccount;
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Acount where email =:email";
        Query query = session.createQuery(hql);
        query.setParameter("email", email);
        try {
            soughtAccount = (Account) query.getSingleResult();
        } catch (NoResultException nre) {
            soughtAccount = null;
        }
        return soughtAccount;
    }

    public Integer getIdForAccountWithEmail(String email){
        return getAccountByEmail(email).getId();
    }

    public Account findByToken(String token){
        Account account;
        Session session = sessionFactory.getCurrentSession();
        String hql = "from Account where token =:token";
        Query query = session.createQuery(hql);
        query.setParameter("token", token);
        try {
            account = (Account) query.getSingleResult();
        } catch (NoResultException nre) {
            account = null;
        }

        return account;
    }

    public Account changeAccountRole(Account account, AccountRole role){
        if(!account.getEmail().equals(""))
            account =getAccountByEmail(account.getEmail());
        if(!account.getUsername().equals(""))
            account =getAccountByUsername(account.getUsername());
        account.setRole(role);
        update(account);
        return account;
    }

}
