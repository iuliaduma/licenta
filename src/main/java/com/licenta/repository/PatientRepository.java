package com.licenta.repository;

import com.licenta.model.classes.Account;
import com.licenta.model.classes.Patient;
import com.licenta.repository.interfaces.InterfaceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PatientRepository implements InterfaceRepository<Patient, Integer> {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Patient findById(Integer entityId) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Patient.class, entityId);
    }

    @Override
    public List<Patient> getAll() {
        Session session = sessionFactory.getCurrentSession();
        List<Patient> patients = session.createQuery("from Patient", Patient.class).list();
        return patients;
    }

    @Override
    public Integer create(Patient entity) {
        Session session=sessionFactory.getCurrentSession();
        Patient patient=new Patient();
        patient.setAccount(entity.getAccount());
        patient.setCardCode(entity.getCardCode());
        patient.setCNP(entity.getCNP());
        patient.getAccount().setPassword(bCryptPasswordEncoder.encode(patient.getAccount().getPassword()));
        patient.setFirstname(entity.getFirstname());
        patient.setImage(entity.getImage());
        patient.setSurname(entity.getSurname());
        return (Integer) session.save(patient);
    }

    @Override
    public void update(Patient entity) {
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(entity);
    }

    @Override
    public boolean delete(Integer entityId) {
        Session session= sessionFactory.getCurrentSession();
        Patient patient = session.get(Patient.class, entityId);
        session.delete(patient);
        return !session.contains(patient);
    }

    public Patient getPatient(Account account){
        Session session =sessionFactory.getCurrentSession();
        String hql = "from Patient where accountId =:accountId";
        Query query =session.createQuery(hql);
        query.setParameter("accountId", account.getId());
        return (Patient) query.getSingleResult();
    }

}
