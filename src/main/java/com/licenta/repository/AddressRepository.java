package com.licenta.repository;

import com.licenta.model.classes.Address;
import com.licenta.repository.interfaces.InterfaceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AddressRepository implements InterfaceRepository<Address, Integer> {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Address findById(Integer entityId) {
        Session session=sessionFactory.getCurrentSession();
        return session.get(Address.class, entityId);
    }

    @Override
    public List<Address> getAll() {
       Session session= sessionFactory.getCurrentSession();
       return session.createQuery("from Address", Address.class).list();
    }

    @Override
    public Integer create(Address entity) {
        Session session=sessionFactory.getCurrentSession();
        Address addressToInsert= new Address();
        addressToInsert.setCity(entity.getCity());
        addressToInsert.setCountry(entity.getCountry());
        addressToInsert.setCounty(entity.getCounty());
        addressToInsert.setStreetName(entity.getStreetName());
        addressToInsert.setStreetNumber(entity.getStreetNumber());
        return (Integer) session.save(addressToInsert);
    }

    @Override
    public void update(Address entity) {
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(entity);

    }

    @Override
    public boolean delete(Integer entityId) {
        Session session = sessionFactory.getCurrentSession();
        Address address = session.get(Address.class, entityId);
        session.delete(address);
        return !session.contains(address);
    }
}
