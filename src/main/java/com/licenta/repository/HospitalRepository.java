package com.licenta.repository;

import com.licenta.model.classes.Hospital;
import com.licenta.repository.interfaces.InterfaceRepository;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class HospitalRepository implements InterfaceRepository<Hospital, Integer> {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Hospital findById(Integer entityId) {
        Session session=sessionFactory.getCurrentSession();
        return session.get(Hospital.class,entityId);
    }

    @Override
    public List<Hospital> getAll() {
        Session session=sessionFactory.getCurrentSession();
        return session.createQuery("from Hospital", Hospital.class).list();
    }

    @Override
    public Integer create(Hospital entity) {
        Session session=sessionFactory.getCurrentSession();
        Hospital hospital=new Hospital();
        hospital.setCode(entity.getCode());
        hospital.setAddress(entity.getAddress());
        hospital.setName(entity.getName());
        return (Integer) session.save(hospital);
    }

    @Override
    public void update(Hospital entity) {
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(entity);
    }

    @Override
    public boolean delete(Integer entityId) {
        Session session=sessionFactory.getCurrentSession();
        Hospital hospital=session.get(Hospital.class,entityId);
        session.delete(hospital);
        return !session.contains(hospital);
    }


    public List<Hospital> getHospitals(){
        Session session = sessionFactory.getCurrentSession();
        List<Hospital> hospitals = session.createQuery("from Hospital", Hospital.class).list();
        return hospitals;
    }

    public List<Hospital> getHospitalsFilterByCounty(String county){
        Session session = sessionFactory.getCurrentSession();
        String hql = "select H.* from Hospital as H inner join Addres as A where H.addressId = A.addressId and A.county =: county";
        Query query = session.createQuery(hql);
        query.setParameter("county", county);
        List<Hospital> hospitals = query.getResultList();
        return hospitals;
    }

    public List<Hospital> getHospitalsFilterByCity(String city){
        Session session = sessionFactory.getCurrentSession();
        String hql = "select H.* from Hospital as H inner join Addres as A where H.addressId = A.addressId and A.city =: city";
        Query query = session.createQuery(hql);
        query.setParameter("city", city);
        List<Hospital> hospitals = query.getResultList();
        return hospitals;
    }

    public Hospital getHospitalByCode(String code){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Hospital where code =:code");
        query.setParameter("code", code);
        return (Hospital) query.getSingleResult();
    }
}
