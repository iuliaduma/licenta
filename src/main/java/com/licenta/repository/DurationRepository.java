package com.licenta.repository;

import com.licenta.model.classes.Duration;
import com.licenta.repository.interfaces.InterfaceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DurationRepository implements InterfaceRepository<Duration, Integer> {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Duration findById(Integer entityId) {
        Session session=sessionFactory.getCurrentSession();
        return session.get(Duration.class, entityId);
    }

    @Override
    public List<Duration> getAll() {
        Session session=sessionFactory.getCurrentSession();
        return session.createQuery("from Duration", Duration.class).list();
    }

    @Override
    public Integer create(Duration entity) {
        Session session=sessionFactory.getCurrentSession();
        Duration duration=new Duration();
        duration.setDoctorType(entity.getDoctorType());
        duration.setDuration(entity.getDuration());
        return (Integer) session.save(duration);
    }

    @Override
    public void update(Duration entity) {
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(entity);

    }

    @Override
    public boolean delete(Integer entityId) {
        Session session=sessionFactory.getCurrentSession();
        Duration duration=session.get(Duration.class,entityId);
        session.delete(duration);
        return !session.contains(duration);
    }
}
