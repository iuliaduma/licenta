package com.licenta.filter;

import com.licenta.facade.AuthenticationFacade;
import com.licenta.model.classes.Account;
import com.licenta.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AccountFilter implements Filter {

    @Autowired
    private AccountService accountService;
    @Autowired
    private AuthenticationFacade authenticationFacade;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext servletContext = filterConfig.getServletContext();
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        AutowireCapableBeanFactory autowireCapableBeanFactory = webApplicationContext.getAutowireCapableBeanFactory();
        autowireCapableBeanFactory.configureBean(this, "accountService");

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(true);
        boolean cookieFound = false;
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie ck : cookies) {
                    if ("email".equals(ck.getName())) {
                        ck.setMaxAge(1209600);
                        Account account = accountService.getAccount(ck.getValue());
                        if (account != null) {
                            session.setAttribute("account", account);
                            cookieFound = true;
                        }
                    }
                }


            }
            if(!cookieFound) {
                Account account = accountService.getAccount(authenticationFacade.verifyIfAuthenticatedAndThenRetrieveName());
                if(account != null){
                    session.setAttribute("account", account);
                }
                //session.setAttribute("account", null);
                Cookie cookie = new Cookie("email", authenticationFacade.verifyIfAuthenticatedAndThenRetrieveName());
                cookie.setMaxAge(1209600);
                response.addCookie(cookie);
            }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
