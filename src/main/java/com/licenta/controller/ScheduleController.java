package com.licenta.controller;

import com.licenta.dto.DoctorDetailsDTO;
import com.licenta.facade.ScheduleFacade;
import com.licenta.model.classes.Schedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/schedules")
public class ScheduleController {

    @Autowired
    private ScheduleFacade facade;

    @RequestMapping(value = "/familyDoctorSchedule", method = RequestMethod.GET)
    public String getFamilyDoctorSchedule(Model model){
        String user /*= (String) session.getAttribute("email")*/;
        user = "admin";
        Schedule schedule = facade.getFamilyDoctorSchedule(user);
        model.addAttribute("schedule", schedule);
        return "doctorSchedule";
    }

    @RequestMapping(value = "/doctorSchedule", method = RequestMethod.GET)
    public String getDoctorSchedule(Model model/*,@RequestParam DoctorDetailsDTO doctor*/){
        String user /*= (String) session.getAttribute("email")*/;
        user = "admin";
        DoctorDetailsDTO doctor = new DoctorDetailsDTO();
        doctor.setFirstname("aaaa");
        doctor.setSurname("aaa");
        Schedule schedule = facade.getDoctorSchedule(doctor);
        model.addAttribute("schedule", schedule);
        return "doctorSchedule";
    }

}
