package com.licenta.controller;

import com.licenta.dto.HospitalDTO;
import com.licenta.facade.HospitalFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/hospitals")
public class HospitalController {

    @Autowired
    private HospitalFacade facade;

    @RequestMapping(value = "/")
    public String getHospitals(Model model){
        model.addAttribute("hospitals", facade.getHospitals());
        return "viewHospitalList";
    }

    @RequestMapping(value = "/hospital/{id}", method = RequestMethod.GET)
    public String getDoctorsFilterByHospital(Model model, @PathVariable String id){
        model.addAttribute("doctors", facade.getDoctorsFilterByHospital(Integer.parseInt(id)));
        return "viewDoctorList";
    }

    @RequestMapping(value = "/addHospital", method = RequestMethod.GET)
    public String viewAddHospital(Model model){
        model.addAttribute("hospital", new HospitalDTO());
        return "addHospital";
    }

    @RequestMapping(value = "/addHospital")
    public String addHospital(@ModelAttribute(name = "hospital") HospitalDTO hospitalDTO){
        facade.create(hospitalDTO);
        return "redirect:/hospitals/";
    }
}
