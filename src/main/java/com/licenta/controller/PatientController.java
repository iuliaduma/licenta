package com.licenta.controller;

import com.licenta.dto.PatientAddDTO;
import com.licenta.dto.ProgrammingDTO;
import com.licenta.dto.RecipeDTO;
import com.licenta.facade.AuthenticationFacade;
import com.licenta.facade.PatientFacade;
import com.licenta.model.classes.UserForm;
import com.licenta.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.util.List;


@Controller
@RequestMapping("/patients")
public class PatientController {

    @Autowired
    private PatientFacade facade;
    @Autowired
    private UserValidator userValidator;
    @Autowired
    private AuthenticationFacade authenticationFacade;

    @RequestMapping(value = "/", method =  RequestMethod.GET)
    public String getMenu(){
        return "patientMenu";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String viewAddPatient(Model model){
        model.addAttribute("patient", new PatientAddDTO());
        return "addPatient";
    }

    @RequestMapping(value = "/signup")
    public String post(@ModelAttribute(name = "patient") PatientAddDTO patientAddDTO,
                       @RequestParam(name = "passwordConfirmation") String passwordConfirmation,
                       BindingResult bindingResult, Model model){
        String goToPage;
        UserForm userForm = new UserForm();
        userForm.setPasswordConfirmation(passwordConfirmation);
        userForm.setUser(patientAddDTO.getAccount());
        /*userValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
           model.addAttribute("errors", bindingResult.getAllErrors());
            goToPage = "addPatient";
        } else {*/
              facade.add(patientAddDTO);
           goToPage = "redirect:/login";
        //}
        return goToPage;
    }

    @RequestMapping(value = "/programmingList", method = RequestMethod.GET)
    public String getProgrammings(Model model, @RequestParam(required = false) String doctorName, HttpSession session,
                                  @RequestParam(required = false) String doctorType,
                                  @RequestParam(required = false) Date startDate,
                                  @RequestParam(required = false) Date endDate){
        //accountDTO.setRole(AccountRole.PATIENT);
        String user = authenticationFacade.verifyIfAuthenticatedAndThenRetrieveName();
            List<ProgrammingDTO> programmings = facade.getProgrammings(user, null, null, null,null);
        //List<ProgrammingDTO> programmings = facade.getProgrammings(user, doctorName, DoctorType.valueOf(doctorType), startDate,endDate);
        model.addAttribute("programmings", programmings);
        return "viewProgrammings";
    }

    @RequestMapping(value = "/programmingList/{name}", method = RequestMethod.GET)
    public String getProgrammingDetails(Model model, @PathVariable(name = "name") String name){
        return "viewDocuments";
    }

    @RequestMapping(value = "/recipes", method = RequestMethod.GET)
    public String getRecipes(Model model, @RequestParam(required = false) String doctorName, HttpSession session,
                             @RequestParam(required = false) String doctorType,
                             @RequestParam(required = false) Date startDate,
                             @RequestParam(required = false) Date endDate){
        String user = authenticationFacade.verifyIfAuthenticatedAndThenRetrieveName();
//        List<RecipeDTO> recipes = facade.getRecipes(user, doctorName, DoctorType.valueOf(doctorType), startDate, endDate);
        List<RecipeDTO> recipes = facade.getRecipes(user, null, null,null, null);
        model.addAttribute("recipes", recipes);
        return "viewRecipes";
    }

    @RequestMapping(value = "/recipes/{name}", method = RequestMethod.GET)
    public String getRecipeDetails(Model model, @PathVariable(name = "name") String name){
        return "viewDocuments";
    }

}
