package com.licenta.controller;

import com.licenta.dto.RecipeDTO;
import com.licenta.facade.RecipeFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/recipes")
public class RecipeController {

    @Autowired
    private RecipeFacade facade;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddRecipe(Model model){
        model.addAttribute("recipe", new RecipeDTO());
        model.addAttribute("doctors", facade.getDoctors());
        return "addRecipe";
    }

    @RequestMapping(value = "/add")
    public String addRecipe(@ModelAttribute(name = "recipe") RecipeDTO recipe){
        facade.add(recipe);
        return "redirect:/patients/viewRecipes";
    }
}
