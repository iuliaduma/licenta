package com.licenta.controller;

import com.licenta.dto.AccountDTO;
import com.licenta.dto.DoctorAddDTO;
import com.licenta.facade.CreateUserFacade;
import com.licenta.model.classes.Account;
import com.licenta.model.classes.UserForm;
import com.licenta.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/user")
public class CreateUserController {

    @Autowired
    private CreateUserFacade createUserFacade;

    @Autowired
    private UserValidator userValidator;

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String getUserForm(Model model) {
        model.addAttribute("doctor",new DoctorAddDTO());
        //model.addAttribute("hospitals", createUserFacade.getAllHospitals());
        return "addDoctor";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getAdminMenu(Model model){
        return "adminMenu";
    }

    @RequestMapping(value = "/signup")
    public String createUser(@ModelAttribute(name = "doctor") DoctorAddDTO doctor) {
          createUserFacade.addDoctor(doctor);
        return "redirect:/user/";
    }

    @RequestMapping(value = "/get")
    public List<Account> getAll(){
        return  createUserFacade.getAll();
    }

    @RequestMapping(value = "/addAdminUser", method = RequestMethod.GET)
    public String accesDoctor(Model model) {
        model.addAttribute("user", new AccountDTO());
        return "addAdmin";
    }

    @RequestMapping(value = "/addAdminUser")
    public String addDoctor(@ModelAttribute(name = "user") AccountDTO accountDTO,
                                    Model model) {
        String goToPage;
        UserForm userForm = new UserForm();
            createUserFacade.createAdminAccount(accountDTO);
            goToPage = "redirect:/login";
        return goToPage;
    }

    @RequestMapping(value = "/confirm/{token}")
    public String confirmRegistration (@PathVariable String token){
        String goToPage;
        boolean validatedWithSuccess;

        validatedWithSuccess = createUserFacade.enableAccount(token);
        if (validatedWithSuccess){
            goToPage = "redirect:/login";
        } else {
            goToPage = "validationError";
        }

        return goToPage;
    }
}