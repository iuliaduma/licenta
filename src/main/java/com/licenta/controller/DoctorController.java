package com.licenta.controller;

import com.licenta.dto.DoctorDetailsDTO;
import com.licenta.dto.DoctorFeedbackDTO;
import com.licenta.facade.DoctorFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/doctors")
public class DoctorController {

    @Autowired
    private DoctorFacade doctorFacade;

    @RequestMapping(value = "/", method =  RequestMethod.GET)
    public String getMenu(){
        return "doctorMenu";
    }

    @RequestMapping("/doctor/{cui}")
    public String getDoctor(Model model, @PathVariable String cui){
            DoctorDetailsDTO doctor = doctorFacade.getDoctor(cui);
        model.addAttribute("doctor", doctor);
        System.out.println(doctor.getCui());
        model.addAttribute("feedback", new DoctorFeedbackDTO());
        model.addAttribute("imgString", doctor.getImage().getOriginalFilename());
        return "doctorProfile";
    }

    @RequestMapping(value = "/doctor/{cui}", method = RequestMethod.POST)
    public String addReview(@ModelAttribute(name = "feedback") DoctorFeedbackDTO feedbackDTO, Model model, @PathVariable(name = "cui") String cui, HttpServletRequest request){
        feedbackDTO.setRating(Integer.parseInt(request.getParameter("rating")));
        doctorFacade.addFeedback(feedbackDTO, cui);
        return "redirect:/doctors/doctor/"+cui;
    }

    @RequestMapping("/viewDoctorList")
    public String getDoctors(Model model){
        model.addAttribute("doctors", doctorFacade.getDoctors());
        return "viewDoctorList";
    }
}
