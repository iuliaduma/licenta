package com.licenta.controller;

import com.licenta.facade.AuthenticationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/security")
public class SecurityController {
    @Autowired
    private AuthenticationFacade authenticationFacade;

    @RequestMapping(value = "/username", method = RequestMethod.GET)
    public void currentUserNameSimple() {
        String name = authenticationFacade.verifyIfAuthenticatedAndThenRetrieveName();
        System.out.println(name);
    }
}
