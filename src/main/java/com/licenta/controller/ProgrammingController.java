package com.licenta.controller;

import com.licenta.dto.ProgrammingDTO;
import com.licenta.facade.ProgrammingFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/programmings")
public class ProgrammingController {

    @Autowired
    private ProgrammingFacade programmingFacade;

    @RequestMapping(value = "/")
    public String viewDoctorList(Model model){
        model.addAttribute("doctors", programmingFacade.getDoctors());
        return "viewDoctorListForProgramming";
    }

    @RequestMapping(value = "/doctor/{cui}", method = RequestMethod.GET)
    public String getDate(@PathVariable String cui, Model model){
        model.addAttribute("programming", new ProgrammingDTO());
        model.addAttribute("doctor", programmingFacade.getDoctor(cui));
        return "addProgramming";
    }


    @RequestMapping(value = "/addProgramming", method = RequestMethod.GET)
    public String viewAddProgramming(Model model){
        model.addAttribute("programming", new ProgrammingDTO());
        model.addAttribute("doctors", programmingFacade.getDoctors());
        return "addProgramming";
    }

    @RequestMapping(value = "/addProgramming")
    public String addProgramming(@ModelAttribute(name = "programming") ProgrammingDTO programmingDTO){
        programmingFacade.create(programmingDTO);
        return "redirect:/patients/programmingList";
    }
}
