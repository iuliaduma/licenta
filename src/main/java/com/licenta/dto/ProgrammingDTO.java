package com.licenta.dto;

import com.licenta.model.classes.Patient;

import java.sql.Date;
import java.sql.Time;

public class ProgrammingDTO {
    private String name;
    private DoctorDetailsDTO doctor;
    private Time hour;
    //private Duration duration;
    private int duration;
    private Patient patient;
    private Date data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DoctorDetailsDTO getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorDetailsDTO doctor) {
        this.doctor = doctor;
    }

    public Time getHour() {
        return hour;
    }

    public void setHour(Time hour) {
        this.hour = hour;
    }

    /*public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }*/

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
