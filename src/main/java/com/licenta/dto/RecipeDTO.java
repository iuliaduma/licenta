package com.licenta.dto;

import java.sql.Date;

public class RecipeDTO {
    private String name;
    private DoctorDetailsDTO doctor;
    private PatientAddDTO patient;
    private Date data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DoctorDetailsDTO getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorDetailsDTO doctor) {
        this.doctor = doctor;
    }

    public PatientAddDTO getPatient() {
        return patient;
    }

    public void setPatient(PatientAddDTO patient) {
        this.patient = patient;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
