package com.licenta.dto;

import com.licenta.model.enums.DoctorType;

import java.util.ArrayList;
import java.util.List;

public class DoctorAddDTO extends UserAddDTO {
    private String cui;
    private DoctorType doctorType;
    private HospitalDTO hospital;
    private List<DocumentDTO> documents = new ArrayList<DocumentDTO>();

    public String getCui() {
        return cui;
    }

    public void setCui(String cui) {
        this.cui = cui;
    }

    public DoctorType getDoctorType() {
        return doctorType;
    }

    public void setDoctorType(DoctorType doctorType) {
        this.doctorType = doctorType;
    }

    public HospitalDTO getHospital() {
        return hospital;
    }

    public void setHospital(HospitalDTO hospital) {
        this.hospital = hospital;
    }

    public List<DocumentDTO> getDocuments() {
        return documents;
    }

    public void setDocuments(List<DocumentDTO> documents) {
        this.documents = documents;
    }

}
