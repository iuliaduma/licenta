package com.licenta.dto;

import com.licenta.model.classes.Document;
import com.licenta.model.enums.DoctorType;

import java.util.ArrayList;
import java.util.List;

public class DoctorViewProfileDTO {
    private String firstname;
    private String surname;
    private AccountDTO account;
    private DoctorType doctorType;
    private String image = "default.jpg";
    private List<Document> documents = new ArrayList<Document>();

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public AccountDTO getAccount() {
        return account;
    }

    public void setAccount(AccountDTO account) {
        this.account = account;
    }

    public DoctorType getDoctorType() {
        return doctorType;
    }

    public void setDoctorType(DoctorType doctorType) {
        this.doctorType = doctorType;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }
}
