package com.licenta.dto;

import com.licenta.model.enums.DoctorType;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public class DoctorDetailsDTO {

    private String cui;
    private String firstname;
    private String surname;
    private DoctorType doctorType;
    private MultipartFile image ;
    private List<DocumentDTO> documents;
    private List<DoctorFeedbackDTO> feedbacks;

    public String getCui() {
        return cui;
    }

    public void setCui(String cui) {
        this.cui = cui;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public DoctorType getDoctorType() {
        return doctorType;
    }

    public void setDoctorType(DoctorType doctorType) {
        this.doctorType = doctorType;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public List<DocumentDTO> getDocuments() {
        return documents;
    }

    public void setDocuments(List<DocumentDTO> documents) {
        this.documents = documents;
    }

    public List<DoctorFeedbackDTO> getFeedbacks() {
        return feedbacks;
    }

    public void setFeedbacks(List<DoctorFeedbackDTO> feedbacks) {
        this.feedbacks = feedbacks;
    }
}
