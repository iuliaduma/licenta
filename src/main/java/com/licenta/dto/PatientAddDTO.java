package com.licenta.dto;

import com.licenta.model.classes.Document;

import java.util.ArrayList;
import java.util.List;

public class PatientAddDTO extends UserAddDTO{

    private List<Document> documents = new ArrayList<Document>();

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }
}
