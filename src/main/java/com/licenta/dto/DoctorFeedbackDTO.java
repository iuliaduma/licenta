package com.licenta.dto;

public class DoctorFeedbackDTO {
    private UserLoginDTO patient;
    private String text;
    private int rating;

    public UserLoginDTO getPatient() {
        return patient;
    }

    public void setPatient(UserLoginDTO patient) {
        this.patient = patient;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
