package com.licenta.dto;

import com.licenta.model.enums.AccountRole;

public class AccountDTO {
    private String username;
    private String email;
    private String password;
    private AccountRole accountRole;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AccountRole getRole() {
        return accountRole;
    }

    public void setRole(AccountRole accountRole) {
        this.accountRole = accountRole;
    }
}
