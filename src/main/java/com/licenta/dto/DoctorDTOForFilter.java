package com.licenta.dto;

import com.licenta.model.enums.DoctorType;

public class DoctorDTOForFilter {

    private String user;
    private DoctorType type;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public DoctorType getType() {
        return type;
    }

    public void setType(DoctorType type) {
        this.type = type;
    }
}
