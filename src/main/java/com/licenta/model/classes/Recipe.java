package com.licenta.model.classes;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "Recipe")
public class Recipe implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int recipeId;
    @Column(name = "name", unique = true)
    private String name;
    @ManyToOne
    @JoinColumn(name = "doctorId")
    private Doctor doctor;
    @ManyToOne
    @JoinColumn(name = "patientId")
    private Patient patient;
    @Column(name = "date")
    private Date data;


    public int getId() {
        return recipeId;
    }

    public void setId(int recipeId) {
        this.recipeId = recipeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "recipeId=" + recipeId +
                ", name='" + name + '\'' +
       //         ", doctor=" + doctor +
         //       ", patient=" + patient +
                ", data=" + data +
                '}';
    }
}
