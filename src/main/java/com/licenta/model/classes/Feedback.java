package com.licenta.model.classes;

import com.licenta.model.enums.FeedbackType;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Feedback")
public class Feedback implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO)
    private int feedbackId;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn( name = "patientId")
    private Patient patient;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "doctorId")
    private Doctor doctor;
    @ManyToOne
    @JoinColumn(name = "hospitalId")
    private Hospital hospital;
    @Column(name = "feedbackType")
    @Enumerated(EnumType.STRING)
    private FeedbackType feedbackType;
    @Column(name = "text")
    private String text;
    @Column(name = "rating")
    private int rating;

    public int getId(){return feedbackId;}

    public void setId(int id) {feedbackId=id;}

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public FeedbackType getFeedbackType() {
        return feedbackType;
    }

    public void setFeedbackType(FeedbackType feedbackType) {
        this.feedbackType = feedbackType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "feedbackId=" + feedbackId +
                ", feedbackType=" + feedbackType +
                ", text='" + text + '\'' +
                ", rating=" + rating +
                '}';
    }
}
