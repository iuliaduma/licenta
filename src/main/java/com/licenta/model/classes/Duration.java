package com.licenta.model.classes;

import com.licenta.model.enums.DoctorType;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Duration")
public class Duration implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int durationId;
    @Column(name = "duration")
    private int duration;
    @Column(name = "doctorType")
    @Enumerated(EnumType.STRING)
    private DoctorType doctorType;

    public int getId() {
        return durationId;
    }

    public void setId(int durationId) {
        this.durationId = durationId;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public DoctorType getDoctorType() {
        return doctorType;
    }

    public void setDoctorType(DoctorType doctorType) {
        this.doctorType = doctorType;
    }

    @Override
    public String toString() {
        return "Duration{" +
                "durationId=" + durationId +
                ", duration=" + duration +
                ", doctorType=" + doctorType +
                '}';
    }
}
