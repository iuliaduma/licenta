package com.licenta.model.classes;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

@Entity
@Table(name="Programming")
public class Programming implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int programmingId;
    @Column(name = "name", unique = true)
    private String name;
    @ManyToOne
    @JoinColumn(name = "doctorId")
    private Doctor doctor;;
    @Column(name = "hour")
    private Time hour;
    /*@ManyToOne
    @JoinColumn(name = "durationId")
    private Duration duration;*/
    private int duration;
    @ManyToOne
    @JoinColumn(name = "patientId")
    private Patient patient;
    @Column(name = "date")
    private Date data;


    public int getId() {
        return programmingId;
    }

    public void setId(int programmingId) {
        this.programmingId = programmingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Time getHour() {
        return hour;
    }

    public void setHour(Time hour) {
        this.hour = hour;
    }

    /*public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }*/

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Programming{" +
                "programmingId=" + programmingId +
                ", name='" + name + '\'' +
     //           ", doctor=" + doctor +
                ", hour=" + hour +
                ", duration=" + duration +
    //            ", patient=" + patient +
                ", data=" + data +
                '}';
    }
}
