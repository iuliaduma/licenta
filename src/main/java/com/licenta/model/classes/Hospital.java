package com.licenta.model.classes;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Hospital")
public class Hospital implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int hospitalId;
    @Column(name = "code")
    private String code;
    @Column(name = "name")
    private String name;
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "addressId")
    private Address address;
    /*@OneToMany(fetch = FetchType.EAGER, mappedBy = "hospital")
    @JsonManagedReference
    private List<Doctor> doctors;*/

    public int getId() {
        return hospitalId;
    }

    public void setId(int spitalId) {
        this.hospitalId = spitalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address  address) {
        this.address = address;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /*public List<Doctor> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<Doctor> doctors) {
        this.doctors = doctors;
    }*/

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalId=" + hospitalId +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", address=" + address +
                '}';
    }
}
