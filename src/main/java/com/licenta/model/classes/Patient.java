package com.licenta.model.classes;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Patient")
public class Patient extends User{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int patientId;
    @ManyToOne
    @JoinColumn(name = "accountId")
    @Cascade(value=org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private Account account;
    @Column(name = "cardCode", unique = true)
    private String cardCode;
    @Column(name ="firstname")
    private String firstname;
    @Column(name = "surname")
    private String surname;
    @Column(name = "cnp",unique = true)
    private String CNP;
    @Column(name = "image")
    private String image = "default.jpg";
    @OneToMany(mappedBy = "patient")
    //@JoinColumn(name = "document")
    private List<Programming> programmings = new ArrayList<>();
    @OneToMany(mappedBy = "patient")
    private List<Recipe> recipes = new ArrayList<>();
    @ManyToOne
    @JoinColumn(name = "doctorId")
    private Doctor doctor;

    public int getId() {
        return patientId;
    }

    public void setId(int patientId) {
        this.patientId = patientId;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Programming> getProgrammings() {
        return programmings;
    }

    public void setProgrammings(List<Programming> programmings) {
        this.programmings = programmings;
    }

    public List<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }

    public Doctor getFamilyDoctor() {
        return doctor;
    }

    public void setFamilyDoctor(Doctor familyDoctor) {
        this.doctor = familyDoctor;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "patientId=" + patientId +
                ", account=" + account +
                ", cardCode='" + cardCode + '\'' +
                ", firstname='" + firstname + '\'' +
                ", surname='" + surname + '\'' +
                ", CNP='" + CNP + '\'' +
                ", image='" + image + '\'' +
            //    ", familyDoctor=" + doctor +
                '}';
    }
}
