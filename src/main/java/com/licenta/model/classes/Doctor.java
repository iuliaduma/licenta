package com.licenta.model.classes;

import com.licenta.model.enums.DoctorType;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Doctor")
public class Doctor extends User{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int doctorId;
    @Column(name = "CUI", unique = true)
    private String cui;
    @Column(name = "cardCode", unique = true)
    private String cardCode;
    @Column(name ="firstname")
    private String firstname;
    @Column(name = "surname")
    private String surname;
    @Column(name = "cnp",unique = true)
    private String CNP;
    @ManyToOne
    //@JsonManagedReference
    @JoinColumn(name = "accountId")
    @Cascade(value=org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private Account account;
    @Column(name = "doctorType")
    @Enumerated(EnumType.STRING)
    private DoctorType doctorType;
    @Column(name = "image")
    private String image = "default.jpg";
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "doctor")
    //@JsonManagedReference
    private List<Document> documents = new ArrayList<Document>();
    @ManyToOne
    @JoinColumn(name = "hospitalId")
    private Hospital hospital;
    @ManyToOne
    @JoinColumn(name = "scheduleId")
    private Schedule schedule;


    public int getId() {
        return doctorId;
    }

    public void setId(int doctorId) {
        this.doctorId = doctorId;
    }

    public String getCUI() {
        return cui;
    }

    public void setCUI(String cui) {
        this.cui = cui;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public DoctorType getDoctorType() {
        return doctorType;
    }

    public void setDoctorType(DoctorType doctorType) {
        this.doctorType = doctorType;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public String getImage(){ return image;}

    public void  setImage(String image) { this.image=image;}

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "doctorId=" + doctorId +
                ", cui='" + cui + '\'' +
                ", cardCode='" + cardCode + '\'' +
                ", firstname='" + firstname + '\'' +
                ", surname='" + surname + '\'' +
                ", CNP='" + CNP + '\'' +
                ", account=" + account +
                ", doctorType=" + doctorType +
                ", image='" + image + '\'' +
                '}';
    }
}
