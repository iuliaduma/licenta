package com.licenta.model.classes;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="Document")
public class Document implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int documentId;
    @Column(name = "name", unique = true)
    private String name;
    @ManyToOne
    @JoinColumn(name = "doctorId")
    private Doctor doctor;

    public int getId() {
        return documentId;
    }

    public void setId(int documentId) {
        this.documentId = documentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    @Override
    public String toString() {
        return "Document{" +
                "documentId=" + documentId +
                ", name='" + name + '\'' +
                ", doctor=" + doctor +
                '}';
    }
}
