package com.licenta.model.classes;

import com.licenta.dto.AccountDTO;

public class UserForm {
    private AccountDTO user;
    private String passwordConfirmation;

    public UserForm() {
        user = null;
        passwordConfirmation = null;
    }

    public AccountDTO getUser() {
        return user;
    }

    public void setUser(AccountDTO user) {
        this.user = user;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    @Override
    public String toString() {
        return "UserForm{" +
                "user=" + user +
                ", passwordConfirmation='" + passwordConfirmation + '\'' +
                '}';
    }
}