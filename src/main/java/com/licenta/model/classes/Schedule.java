package com.licenta.model.classes;

import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.List;

@Entity
@Table(name = "Schedule")
public class Schedule implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int scheduleId;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "schedule")
    @JsonManagedReference
    @Cascade(value=org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private List<Doctor> doctor;
    @Column(name = "startHourMonday")
    private Time startHourMonday;
    @Column(name = "stopHourMonday")
    private Time StopHourMonday;
    @Column(name = "startHourTuesday")
    private Time startHourTuesday;
    @Column(name = "stopHourTuesday")
    private Time stopHourTuesday;
    @Column(name = "startHourWednesday")
    private Time startHourWednesday;
    @Column(name = "stopHourWednesday")
    private Time stopHourWednesday;
    @Column(name = "startHourThursday")
    private Time startHourThursday;
    @Column(name = "stopHourThursday")
    private Time stopHourThursday;
    @Column(name = "startHourFriday")
    private Time startHourFriday;
    @Column(name = "stopHourFriday")
    private Time stopHourFriday;

    public int getId() {return scheduleId;}

    public void setId(int id) {scheduleId=id;}

    public List<Doctor> getDoctor() {
        return doctor;
    }

    public void setDoctor(List<Doctor> doctor) {
        this.doctor = doctor;
    }

    public Time getStartHourMonday() {
        return startHourMonday;
    }

    public void setStartHourMonday(Time startHourMonday) {
        this.startHourMonday = startHourMonday;
    }

    public Time getStopHourMonday() {
        return StopHourMonday;
    }

    public void setStopHourMonday(Time stopHourMonday) {
        StopHourMonday = stopHourMonday;
    }

    public Time getStartHourTuesday() {
        return startHourTuesday;
    }

    public void setStartHourTuesday(Time startHourTuesday) {
        this.startHourTuesday = startHourTuesday;
    }

    public Time getStopHourTuesday() {
        return stopHourTuesday;
    }

    public void setStopHourTuesday(Time stopHourTuesday) {
        this.stopHourTuesday = stopHourTuesday;
    }

    public Time getStartHourWednesday() {
        return startHourWednesday;
    }

    public void setStartHourWednesday(Time startHourWednesday) {
        this.startHourWednesday = startHourWednesday;
    }

    public Time getStopHourWednesday() {
        return stopHourWednesday;
    }

    public void setStopHourWednesday(Time stopHourWednesday) {
        this.stopHourWednesday = stopHourWednesday;
    }

    public Time getStartHourThursday() {
        return startHourThursday;
    }

    public void setStartHourThursday(Time startHourThursday) {
        this.startHourThursday = startHourThursday;
    }

    public Time getStopHourThursday() {
        return stopHourThursday;
    }

    public void setStopHourThursday(Time stopHourThursday) {
        this.stopHourThursday = stopHourThursday;
    }

    public Time getStartHourFriday() {
        return startHourFriday;
    }

    public void setStartHourFriday(Time startHourFriday) {
        this.startHourFriday = startHourFriday;
    }

    public Time getStopHourFriday() {
        return stopHourFriday;
    }

    public void setStopHourFriday(Time stopHourFriday) {
        this.stopHourFriday = stopHourFriday;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "scheduleId=" + scheduleId +
                ", startHourMonday=" + startHourMonday +
                ", StopHourMonday=" + StopHourMonday +
                ", startHourTuesday=" + startHourTuesday +
                ", stopHourTuesday=" + stopHourTuesday +
                ", startHourWednesday=" + startHourWednesday +
                ", stopHourWednesday=" + stopHourWednesday +
                ", startHourThursday=" + startHourThursday +
                ", stopHourThursday=" + stopHourThursday +
                ", startHourFriday=" + startHourFriday +
                ", stopHourFriday=" + stopHourFriday +
                '}';
    }
}
