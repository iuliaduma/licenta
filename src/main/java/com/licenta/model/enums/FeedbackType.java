package com.licenta.model.enums;

public enum FeedbackType {
    DOCTOR,
    HOSPITAL,
    LABORATORY
}
