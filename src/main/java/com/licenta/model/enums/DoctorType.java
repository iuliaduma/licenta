package com.licenta.model.enums;

public enum DoctorType {
    NONE,
    FAMILY_DOCTOR,
    SPECIALIST,
    LABORATORY_ANALYSIS
}
