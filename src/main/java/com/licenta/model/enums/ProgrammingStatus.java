package com.licenta.model.enums;

public enum  ProgrammingStatus {
    UNSCHEDULED,
    WAITING,
    DONE,
    CANCELLED
}
