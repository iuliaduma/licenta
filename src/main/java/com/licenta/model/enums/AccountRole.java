package com.licenta.model.enums;

public enum AccountRole {
    ADMIN,
    DOCTOR,
    NURSE,
    PATIENT
}
