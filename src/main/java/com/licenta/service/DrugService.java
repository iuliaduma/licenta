package com.licenta.service;

import com.licenta.model.classes.Drug;
import com.licenta.repository.DrugRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DrugService{

    @Autowired
    private DrugRepository repository;

    public Drug findById(Integer entityId) {
        return repository.findById(entityId);
    }

    public List<Drug> getAll() {
        return repository.getAll();
    }

    public void update(Drug drug) { repository.update(drug);}

    public Integer create(Drug drug) {
        return repository.create(drug);
    }

    public boolean delete(Integer entityId) {
        return repository.delete(entityId);
    }

}
