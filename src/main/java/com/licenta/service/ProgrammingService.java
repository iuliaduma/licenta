package com.licenta.service;

import com.licenta.model.classes.Doctor;
import com.licenta.model.classes.Programming;
import com.licenta.model.enums.DoctorType;
import com.licenta.repository.ProgrammingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

@Service
@Transactional
public class ProgrammingService {

    @Autowired
    private ProgrammingRepository repository;

    public Programming findById(Integer entityId) { return repository.findById(entityId);}

    public List<Programming> getAll() {
        return repository.getAll();
    }

    public void update(Programming programming){ repository.update(programming);}

    public Integer create(Programming programming) {
        programming.setName(programming.getPatient().getId()+programming.getData().toString());
        int min = programming.getHour().getMinutes();
        if (min!=30 && min != 0) {
            if(min < 30) {
                if (30 - min <= 15)
                    programming.getHour().setMinutes(0);
                else
                    programming.getHour().setMinutes(30);
            }
            else {
                if (min - 30 <= 15)
                    programming.getHour().setMinutes(30);
                else{
                    programming.getHour().setMinutes(30);
                    programming.getHour().setHours(programming.getHour().getHours()+1);
                }
            }
        }
        return repository.create(programming);
    }

    public boolean delete(Integer entityId) {
        return repository.delete(entityId);
    }

    public List<Programming> getAllProgrammingsByPatient(int patientId){
        return repository.getAllProgrammingsByPatient(patientId);
    }

    public List<Programming> getAllProgrammingsByPatientFilteredByDoctorName(int patientId, Doctor doctor){
        return repository.getAllProgrammingsByPatientFilteredByDoctorName(patientId,doctor);
    }

    public List<Programming> getAllProgrammingsByPatientFilteredByDoctorType(int patientId, DoctorType doctorType){
        return repository.getAllProgrammingsByPatientFilteredByDoctorType(patientId, doctorType);
    }

    public List<Programming> getAllProgrammingsByPatientFilteredByDate(int patientId, Date date){
        return repository.getAllProgrammingsByPatientFilteredByDate(patientId, date);
    }

    public List<Programming> getAllProgrammingsByPatientFilteredByIntervalDate(int patientId, Date startingDate, Date endDate){
        return repository.getAllProgrammingsByPatientFilteredByIntervalDate(patientId, startingDate, endDate);
    }
    public List<Programming> getAllProgrammingsByDoctorFilteredByDate(int doctorId, Date date) {
        return repository.getAllProgrammingsByDoctorFilteredByDate(doctorId, date);
    }

    public List<Programming> getAllProgrammingsByDoctorFilteredByIntervalDate(int doctorId, Date startingDate, Date endDate){
        return repository.getAllProgrammingsByDoctorFilteredByIntervalDate(doctorId, startingDate, endDate);
    }
    }
