package com.licenta.service;

import com.licenta.model.classes.Address;
import com.licenta.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AddressService{

    @Autowired
    private AddressRepository repository;

    public Address findById(Integer entityId) {
        return repository.findById(entityId);
    }

    public List<Address> getAll() {
        return repository.getAll();
    }

    public void update(Address address){ repository.update(address);}

    public Integer create(Address address) {
        return repository.create(address);
    }

    public boolean delete(Integer entityId) {
        return repository.delete(entityId);
    }

}
