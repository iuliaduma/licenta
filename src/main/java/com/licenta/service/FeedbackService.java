package com.licenta.service;

import com.licenta.model.classes.Feedback;
import com.licenta.repository.FeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class FeedbackService{

    @Autowired
    private FeedbackRepository repository;

    public Feedback findById(Integer entityId) {
        return repository.findById(entityId);
    }

    public List<Feedback> getAll() {
        return repository.getAll();
    }

    public Integer create(Feedback entity) {
        return repository.create(entity);
    }

    public void update(Feedback entity) {
        repository.update(entity);
    }

    public boolean delete(Integer entityId) {
        return repository.delete(entityId);
    }

    public List<Feedback> getFeedbacksForDoctor(int doctorId){
        return repository.getFeedbacksForDoctor(doctorId);
    }

    public List<Feedback> getFeedbackForHospital(int hospitalId){
        return repository.getFeedbacksForHospital(hospitalId);
    }
}
