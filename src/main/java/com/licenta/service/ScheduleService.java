package com.licenta.service;

import com.licenta.model.classes.Schedule;
import com.licenta.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ScheduleService {

    @Autowired
    private ScheduleRepository repository;

    public Schedule findById(Integer entityId) { return repository.findById(entityId);}

    public List<Schedule> getAll() {
        return repository.getAll();
    }

    public void update(Schedule schedule){ repository.update(schedule);}

    public Integer create(Schedule schedule) {
        return repository.create(schedule);
    }

    public boolean delete(Integer entityId) {
        return repository.delete(entityId);
    }
}
