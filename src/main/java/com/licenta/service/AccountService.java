package com.licenta.service;

import com.licenta.model.classes.Account;
import com.licenta.repository.AccountRepository;
import com.licenta.utils.SendEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("accountService")
@Transactional
public class AccountService {

    @Autowired
    private AccountRepository repository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public String getRole(String user, String password) {
        Account accountToBeFound;
        if(user.contains("@"))
            accountToBeFound = repository.getAccountByEmail(user);
        else
            accountToBeFound = repository.getAccountByUsername(user);
        if (accountToBeFound != null) {
            if (bCryptPasswordEncoder.matches(password, accountToBeFound.getPassword())) {
                if (accountToBeFound.getEnabled() == 1) {
                    return accountToBeFound.getRole().toString().trim();
                }
            }
        }
        return null;
    }

    public Account getAccount(String user, String password){
        Account accountToBeFound;
        if(user.contains("@"))
            accountToBeFound = repository.getAccountByEmail(user);
        else
            accountToBeFound = repository.getAccountByUsername(user);
        if (accountToBeFound != null) {
            if (bCryptPasswordEncoder.matches(password, accountToBeFound.getPassword())) {
                if (accountToBeFound.getEnabled() == 1) {
                    return accountToBeFound;
                }
            }
        }
        return null;
    }

    public Account getAccount(String user){
        Account accountToBeFound;
        if(user.contains("@"))
            accountToBeFound = repository.getAccountByEmail(user);
        else
            accountToBeFound = repository.getAccountByUsername(user);
        if (accountToBeFound != null) {
            return accountToBeFound;

        }
        return null;
    }

    public String sendConfirmationEmail(Account account){
        String emailAddress = account.getEmail();
        String subject = "Email confirmation";
        String message = "This is your confirmation url:\n http://localhost:8080/user/confirm/" + account.getToken();

        SendEmail email = new SendEmail(emailAddress, subject, message);
        return email.sendEmail();
    }

    public boolean enableAccount(String token){
        Account account = repository.findByToken(token);
        Boolean accountWasEnabled;
        if (account == null){
            accountWasEnabled = false;
        } else {
            account.setEnabled(1);
            repository.update(account);
            accountWasEnabled = true;
        }

        return accountWasEnabled;
    }

    public Account findById(Integer entityId) {
        return repository.findById(entityId);
    }

    public List<Account> getAll() {
        return repository.getAll();
    }


    public Integer create(Account account) {
        return repository.create(account);
    }

    public void update(Account entity) {
        repository.update(entity);
    }

    public boolean delete(Integer entityId) {
        return repository.delete(entityId);
    }

    public Account getAccoutByUsername(String name) {
        return repository.getAccountByUsername(name);
    }

    public Integer getIdForAccountWithUsername(String username) {
        return repository.getIdForAccountWithUsername(username);
    }

    public Account getAccountByEmail(String email) {
        return repository.getAccountByEmail(email);
    }

    public Integer getIdForAccountWithEmail(String email) {
        return repository.getIdForAccountWithEmail(email);
    }

    public boolean isEmailTaken(String email) {
        return repository.getAccountByEmail(email) != null;
    }

    public boolean isUsernameTaken(String email) {
        return repository.getAccountByUsername(email) != null;
    }
}
