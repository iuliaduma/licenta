package com.licenta.service;

import com.licenta.model.classes.Document;
import com.licenta.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DocumentService {

    @Autowired
    private DocumentRepository repository;

    public Document findById(Integer entityId) { return repository.findById(entityId);}

    public List<Document> getAll() {
        return repository.getAll();
    }

    public void update(Document document){
        repository.update(document);
    }

    public Integer create(Document document) {
        return repository.create(document);
    }

    public boolean delete(Integer entityId) {
        return repository.delete(entityId);
    }


}
