package com.licenta.service;

import com.licenta.model.classes.Duration;
import com.licenta.repository.DurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DurationService {

    @Autowired
    private DurationRepository repository;

    public Duration findById(Integer entityId) {
        return repository.findById(entityId);
    }

    public List<Duration> getAll() {
        return repository.getAll();
    }

    public void update(Duration duration) { repository.update(duration);}

    public Integer create(Duration duration) {
        return repository.create(duration);
    }

    public boolean delete(Integer entityId) {
        return repository.delete(entityId);
    }

}
