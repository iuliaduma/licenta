package com.licenta.service;

import com.licenta.model.classes.Account;
import com.licenta.model.classes.Patient;
import com.licenta.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PatientService {

    @Autowired
    private PatientRepository repository;

    public Patient findById(Integer entityId) {
        return repository.findById(entityId);
    }

    public List<Patient> getAll() {
        return repository.getAll();
    }

    public Integer create(Patient entity) {
        return repository.create(entity);
    }

    public void update(Patient entity) {
        repository.update(entity);
    }

    public boolean delete(Integer entityId) {
        return repository.delete(entityId);
    }

    public Patient getPatient(Account account){
        return repository.getPatient(account);
    }
}
