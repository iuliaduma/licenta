package com.licenta.service;

import com.licenta.model.classes.Account;
import com.licenta.model.classes.Doctor;
import com.licenta.model.classes.Schedule;
import com.licenta.model.enums.DoctorType;
import com.licenta.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DoctorService {

    @Autowired
    private DoctorRepository repository;

    public List<Doctor> getFreeDoctors(){
        return repository.getAll();
    }

    public Doctor findById(Integer entityId) {
        return repository.findById(entityId);
    }

    public List<Doctor> getAll() {
        return repository.getAll();
    }

    public Integer create(Doctor entity) {
        return repository.create(entity);
    }

    public void update(Doctor entity) {
        repository.update(entity);
    }

    public boolean delete(Integer entityId) {
        return repository.delete(entityId);
    }

    public Schedule getDoctorWeeklySchedule(int doctor){
        return repository.getDoctorWeeklySchedule(doctor);
    }

    public Doctor getDoctor(Account acount){
        return repository.getDoctor(acount);
    }

    public Doctor getDoctorByName(String user){
        String[] users= user.split(" ");
        return repository.getDoctorByName(users[0], users[1]);
    }

    public Doctor getDoctorByCui(String cui){
        return repository.getDoctorByCui(cui);
    }

    public List<Doctor> getDoctorListFilterByHospital(int hospitalId){
        return repository.getDoctorsFilterByHospital(hospitalId);
    }

    public List<DoctorType> getAllDoctorsType(){
        return repository.getAllDoctorsType();
    }
}
