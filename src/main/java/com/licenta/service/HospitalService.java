package com.licenta.service;

import com.licenta.model.classes.Hospital;
import com.licenta.repository.HospitalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class HospitalService{

    @Autowired
    private HospitalRepository repository;

    public Hospital findById(Integer entityId) { return repository.findById(entityId);}

    public List<Hospital> getAll() {
        List<Hospital> hospitals = repository.getAll();
        return hospitals;
    }

    public void update(Hospital hospital){ repository.update(hospital);}

    public Integer create(Hospital hospital) {
        return repository.create(hospital);
    }

    public boolean delete(Integer entityId) {
        return repository.delete(entityId);
    }

    public List<Hospital> getHospitals(){
        return repository.getHospitals();
    }

    public List<Hospital> getHospitalsFilterByCounty(String county){
        return repository.getHospitalsFilterByCounty(county);
    }

    public List<Hospital> getHospitalFilterByCity(String city){
        return repository.getHospitalsFilterByCity(city);
    }

    public Hospital getHospitalByCode(String code){
        return repository.getHospitalByCode(code);
    }

}
