package com.licenta.service;

import com.licenta.model.classes.Doctor;
import com.licenta.model.classes.Recipe;
import com.licenta.model.enums.DoctorType;
import com.licenta.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

@Service
@Transactional
public class RecipeService  {

    @Autowired
    private RecipeRepository repository;

    public Recipe findById(Integer entityId) { return repository.findById(entityId);}

    public List<Recipe> getAll() {
        return repository.getAll();
    }

    public void update(Recipe recipe){ repository.update(recipe);}

    public Integer create(Recipe recipe) {
        return repository.create(recipe);
    }

    public boolean delete(Integer entityId) {
        return repository.delete(entityId);
    }

    public List<Recipe> getAllRecipesByPatient(int patientId){
        return repository.getAllRecipesByPatient(patientId);
    }

    public List<Recipe> getAllRecipesByPatientFilteredByDoctorName(int patientId, Doctor doctor){
        return repository.getAllRecipesByPatientFilteredByDoctorName(patientId, doctor);
    }

    public List<Recipe> getAllRecipesByPatientFilteredByDoctorType(int patientId, DoctorType doctorType){
        return repository.getAllRecipesByPatientFilteredByDoctorType(patientId, doctorType);
    }

    public List<Recipe> getAllRecipesByPatientFilteredByDate(int patientId, Date date){
        return repository.getAllRecipesByPatientFilteredByDate(patientId, date);
    }

    public List<Recipe> getAllRecipesByPatientFilteredByIntervalDate(int patientId, Date startingDate, Date endDate){
        return repository.getAllRecipesByPatientFilteredByIntervalDate(patientId, startingDate, endDate);
    }
}
