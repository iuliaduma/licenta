package com.licenta.validator;

import com.licenta.model.classes.Doctor;
import com.licenta.model.classes.UserForm;
import com.licenta.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.apache.commons.validator.EmailValidator;

@Component
public class UserValidator implements Validator{

    @Autowired
    private AccountService accountService;

    public UserValidator(){

    }

    private String validateFirstName(String name){
        String msg="";
        if(!name.matches("[A-Z][a-z]*"))
            msg="Invalid first name";
        return msg;
    }

    private String validateLasttName(String name){
        String msg="";
        if(!name.matches("[A-Z][a-z]*"))
            msg="Invalid last name";
        return msg;
    }

    private String validateCNP(String cnp){
        String msg="";
        String validCnp="279146358279";
        if(!cnp.matches("[0-9]*"))
            return "Invalid format of cnp";
        if(cnp.length()!=13){
           return  "Cnp must have 13 digits";
        }
        int sum=0;
        for(int i=0; i<12; i++)
            sum+=(cnp.charAt(i)-48)*(validCnp.charAt(i)-48);
        if(sum%11!=cnp.charAt(12)-48 || (sum%11==10 && cnp.charAt(12)!=1))
            msg="Invalid cnp";
        return msg;
    }

    public String validateEmail(String email){
        String msg="";
        if(!email.matches("[a-z]*_?-?.?[a-z]*[0-9]*@[a-z]*.[a-z]*") || email.equals(""))
            msg="Invalid format of email";
        return msg;
    }

    public String validateUsername(String username){
        String msg="";
        if(!username.matches("[a-z]*_?-?.?[a-z]*[0-9]*") || username.equals(""))
            msg="Invalid format of username";
        return msg;
    }

    public String validate(Doctor doctor){
        String msg="";
        msg+=validateFirstName(doctor.getFirstname());
        msg+=validateLasttName(doctor.getSurname());
        msg+=validateCNP(doctor.getCNP());
        return msg;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }

    @Override
    public void validate(Object objectUser, Errors errors) {
        UserForm userForm = (UserForm) objectUser;
        EmailValidator emailValidator = EmailValidator.getInstance();

        ValidationUtils.rejectIfEmpty(errors, "firstname", "firstname.empty", "Name cannot be empty!");
        ValidationUtils.rejectIfEmpty(errors, "surname", "surname.empty", "Surname cannot be empty!");
        ValidationUtils.rejectIfEmpty(errors, "password", "password.empty", "Password cannot be empty!");
        ValidationUtils.rejectIfEmpty(errors, "email", "email.empty", "Email cannot be empty!");
        ValidationUtils.rejectIfEmpty(errors, "username", "username.empty", "Username cannot be empty!");
        ValidationUtils.rejectIfEmpty(errors, "cardCode", "cardCode.empty", "Card code cannot be empty!");
        ValidationUtils.rejectIfEmpty(errors, "CNP", "CNP.empty", "CNP cannot be empty!");

        if (emailIsTaken(userForm.getUser().getEmail())) {
            errors.rejectValue("email", "email.taken", "This email is already in use!");
        }

        if(!emailValidator.isValid(userForm.getUser().getEmail())){
            errors.rejectValue("email","email.not.valid","This email is not valid");
        }

        if (!passwordsMatch(userForm.getUser().getPassword(), userForm.getPasswordConfirmation())) {
            errors.rejectValue("password", "password.mismatch", "Passwords do not match!");
        }
    }

    private boolean emailIsTaken(String email) {
        return accountService.isEmailTaken(email);
    }

    private boolean passwordsMatch(String password, String confirmPassword) {
        return password.equals(confirmPassword);
    }

}
